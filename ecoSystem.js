//Version: v3.3.4
//Bugfixes, dynamisches Top

/*  
*   alle Funktionen geben "XXX" zurück wenn ein Datenbankfehler vorliegt
*   addEco(author, dataname, amount)    return: null = Wert ist keine Zahl
*   subEco(author, dataname, amount)    return: false = vorhandener Wert nicht hoch genug, null = nicht vorhanden
*   getEco(author, dataname)            return: abgefragten Wert, null = nicht vorhanden
*   setEco(author, dataname, value)
*   lbEco(dataname)                     return: sortiertes Array nach dataname
*   getUserEco(author)                  return: user-Klasse
*   = = = = = = = = = = = = = = = = =
*   Codebeispiele:
*   wenn Bot angesprochen wird "client.user" statt "message.author" verwenden
*   "message.mentions.members.first().user" statt "message.author" wenn @user verwendet wird
*
*   client.eco.addEco(message.author, "xp", 100);
*
*   if(!client.eco.subEco(message.author, "xp", 100)) {
*       nicht genügend "xp" vorhanden
*   }
*
*   var test = client.eco.getEco(message.author, "money");
*
*   client.eco.setEco(message.author, "xp", 100);
*
*   var test = client.eco.lbEco("money");
*
*   var test = client.eco.getUserEco(message.author);
*/

const Discord = require('discord.js');
const client = new Discord.Client();
const fs = require("fs");
const number = Intl.NumberFormat();
const message = require('./events/message');
const config = require("./configs/config.json");
const defaultDatabaseFilepath = `./database/db`;
var databaseFilepath;
var starterMoney = config.starterMoney;
var maxMoney = config.maxMoney;


//#region private functions
function readDatabase(serverID) {
    databaseFilepath = defaultDatabaseFilepath + "-" + serverID + ".json";
    try {
        var rawdata = fs.readFileSync(databaseFilepath);
        return database = JSON.parse(rawdata);
    } catch (error) {
        console.error("datebase error: " + error);
        if(error.code === 'ENOENT') { 
            console.log("database doesn't exist -> creating new database"); 
            createNewDatabase();
            return readDatabase(serverID);
        }
        else {
            return null;
        }
    }
}

function writeDatabase(output) {
    try {
        var data = JSON.stringify(output, null, 2);
        fs.writeFileSync(databaseFilepath, data);
    } catch (error) {
        console.error(error);
    }
}

function createNewDatabase() {
    fs.writeFileSync(databaseFilepath, "{\"data\": [{\"ID\": \"" + config.privileges.botID + "\"}]}");    
}

function newEntry(author, database) {

    var standardEntry = {
        ID: author.id,
        username: author.username,
        money: starterMoney,
        highscore: starterMoney,
        xp: 0,
        prestigexp: 0,
        prestigemoney: 0,
    };

    if(database != null) {
        console.log("new entry for: " + author.username);
        database.data.push(standardEntry);
        return database;
    }
    else {
        console.log("database variable has 'null'");
        return null;
    }
}
//#endregion

//#region public functions
function addEco(serverID, author, dataname, amount) {
    var database = readDatabase(serverID);

    if(database == null) {
        console.log("database error");
        return "XXX";
    }
    
    for(i=0; i<database.data.length; i++) {
        if(database.data[i].ID !== author.id && i == database.data.length -1) {
            database = newEntry(author, database);
        }

        if(database.data[i].ID === author.id) {
            if(database.data[i][dataname] != null) {
                if(!isNaN(database.data[i][dataname])) {
                    database.data[i][dataname] = parseInt(database.data[i][dataname]) + parseInt(amount);
                    if (database.data[i][dataname] > maxMoney) {
                        database.data[i][dataname] = maxMoney;
                    }
                    console.log(database.data[i].username + ": +" + amount + " " + dataname);
                    if(database.data[i].username != author.username) {
                        database.data[i].username = author.username;
                    }
                    break;
                }
                else {
                    console.log("error by adding a value: it is not a number");
                    return null;
                }
            }
            else {
                console.log(dataname + ": was not in the database");
                database.data[i][dataname] = parseInt(amount);
                break;
            }
        
        }
    }
    writeDatabase(database);
    highscore(serverID, author);
}

function subEco(serverID, author, dataname, amount) {
    var database = readDatabase(serverID);

    if(database == null) {
        console.log("database error");
        return "XXX";
    }
    
    for(i=0; i<database.data.length; i++) {
        if(database.data[i].ID !== author.id && i == database.data.length -1) {
            database = newEntry(author, database);
        } 
        if(database.data[i].ID === author.id) {
            if(!isNaN(database.data[i][dataname])) {
                if(database.data[i][dataname] >= amount) {
                    database.data[i][dataname] = parseInt(database.data[i][dataname]) - parseInt(amount);
                    if (database.data[i][dataname] > maxMoney) {
                        database.data[i][dataname] = maxMoney;
                    }
                    console.log(database.data[i].username + ": -" + amount + " " + dataname);
                    if(database.data[i].username != author.username) {
                        database.data[i].username = author.username;
                    }
                    break;
                }
                else {
                    console.log(author.username + " has not enought: " + dataname);
                    return false;
                }
            }
            else {
                console.log("error by subtracting a value: it is not a number");
                return null;
            }
        }
    }
    writeDatabase(database);
}

function lbEco(serverID, dataname) {
    var database = readDatabase(serverID);

    if(database == null) {
        console.log("database error");
        return "XXX";
    }

    var leaderboardDatabase = database.data;

    leaderboardDatabase.splice(leaderboardDatabase.findIndex(findBotID), 1);

    bubbleSort();
    return leaderboardDatabase;

    function bubbleSort() {
        swapped = true;
        while(swapped) {
            swapped = false;
            for(i=1; i<leaderboardDatabase.length; i++) {
                if(parseInt(leaderboardDatabase[i-1][dataname]) < parseInt(leaderboardDatabase[i][dataname])) {
                    let temp = leaderboardDatabase[i];
                    leaderboardDatabase[i] = leaderboardDatabase[i-1];
                    leaderboardDatabase[i-1] = temp;
                    swapped = true;
                }
            }
        }
    }
    
    function findBotID(databaseSearch) {
        return databaseSearch.ID === config.privileges.botID;
    }
}

function setEco(serverID, author, dataname, value) {
    var database = readDatabase(serverID);

    if(database == null){
        console.log("database error");
        return "XXX";
    }

    for(i=0; i<database.data.length; i++) {
        if(database.data[i].ID !== author.id && i == database.data.length -1) {
            database = newEntry(author, database);
        }

        if(database.data[i].ID === author.id) {
            database.data[i][dataname] = value;
            break;
        }
    }
    writeDatabase(database);
}

function getEco(serverID, author, dataname) {
    var database = readDatabase(serverID);

    if(database == null){
        console.log("database error");
        return "XXX";
    }

    for(i=0; i<database.data.length; i++) {
        if(database.data[i].ID !== author.id && i == database.data.length -1) {
            database = newEntry(author, database);
        }

        if(database.data[i].ID === author.id) {
            if(database.data[i][dataname] != null) {
                return database.data[i][dataname];
            }
            else {
                return null;
            }
        }
    }
}

function getUserEco(serverID, author) {
    var database = readDatabase(serverID);

    if(database == null){
        console.log("database error");
        return "XXX";
    }

    for(i=0; i<database.data.length; i++) {
        if(database.data[i].ID === author.id) {
            return database.data[i];
        }
    }
}

function highscore(serverID, author) {
    var tempBalance = getEco(serverID, author, `money`);
    var getHighscore = getEco(serverID, author, `highscore`);
    if (tempBalance > getHighscore) setEco(serverID, author, `highscore`, tempBalance);
    return getEco(serverID, author, `highscore`);
}

function prestigeMoney(serverID, author, amount) {
    var lvl = parseInt(getEco(serverID, author, `prestigemoney`));
    var moneyX = parseInt(config.prestige.moneyX);
    var money = parseInt(amount);
    
    if (lvl > 0) {
        var money = parseInt(money/100*(lvl*moneyX)+money);
    }
    return money.toFixed(0);
}

function prestigeXP(serverID, author, amount) {
    var lvl = parseInt(getEco(serverID, author, `prestigexp`));
    var xpX = parseInt(config.prestige.xpX);
    var xp = parseInt(amount);
    
    if (lvl > 0) {
        var xp = parseInt(xp/100*(lvl*xpX)+xp);
    }
    return xp.toFixed(0);
}
//#endregion

module.exports = {addEco, subEco, lbEco, setEco, getEco, getUserEco, prestigeMoney, prestigeXP};
