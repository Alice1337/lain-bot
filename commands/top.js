module.exports = {
    name: "leaderboard",
    cooldown: 0,
    run (client, message, args, cooldownTime) {
        const Discord = require('discord.js');
        var number = Intl.NumberFormat();
        if (message.channel.type != 'dm') SID = message.guild.id;
        if (message.channel.type == 'dm') SID = 'dm';

        //Variables
        var tempLeaderboard = client.db.lbEco(SID, `money`);
        var leaderboardNames = [];
        var leaderboardValues = [];
        var mode = 0;
        var count = 0;

        //interpret arguments
        if (args[0] != null) args[0] = args[0].toLowerCase();
        if (args[1] != null) args[1] = args[1].toLowerCase();
        //desktop
        count = args[0];
        if (count == null || isNaN(count)) count = 5;
        //mobile
        if (args[0] == 'm') {
            mode = 'mobile';
            count = args[1];
            if (args[1] == null || isNaN(args[1])) count = 5;
        }


        //mobile mode
        if (mode == 'mobile') {
            if (count == null) {
                for(i = 0; i < tempLeaderboard.length; i++) {
                    if (tempLeaderboard[i].username != null) leaderboard.push(`#${i+1} ` + tempLeaderboard[i].username + `: ` + number.format(tempLeaderboard[i].money) + `$`);
                    if (i == 4) break;
                }
                const embed = new Discord.MessageEmbed()
                    .setColor(0x5886ce)
                    .setTitle(`Leaderboard`)
                    .addField(`Top ${i} Ballers`, leaderboard)
                message.channel.send(embed);
                return;
            } else
            var tempLeaderboard = client.db.lbEco(SID, `money`);
                var leaderboard = [];
                for(i = 0; i < tempLeaderboard.length; i++) {
                    if (tempLeaderboard[i].username != null) leaderboard.push(`#${i+1} ` + tempLeaderboard[i].username + `: ` + number.format(tempLeaderboard[i].money) + `$`);
                    if (i == count - 1) break;
                }
                const embed = new Discord.MessageEmbed()
                    .setColor(0x5886ce)
                    .setTitle(`Leaderboard`)
                    .addField(`Top ${i+1} Ballers`, leaderboard)
                message.channel.send(embed);
                return;
        }


        //desktop
        if (count == null) {
            for(i = 0; i < tempLeaderboard.length; i++) {
                if (tempLeaderboard[i].username != null) leaderboardNames.push(`#${i+1} ` + tempLeaderboard[i].username + `: `);
                if (tempLeaderboard[i].username != null) leaderboardValues.push(number.format(tempLeaderboard[i].money) + `$`);
                if (i == 4) break;
            }
            const embed = new Discord.MessageEmbed()
                .setColor(0x5886ce)
                .setTitle(`Leaderboard`)
                .addField(`Top ${i+1} Ballers`, leaderboardNames, true)
                .addField(`\u200b`, leaderboardValues, true)
            message.channel.send(embed);
            return;
        } else
            var tempLeaderboard = client.db.lbEco(SID, `money`);
            for(i = 0; i < tempLeaderboard.length; i++) {
                if (tempLeaderboard[i].username != null) leaderboardNames.push(`#${i+1} ` + tempLeaderboard[i].username + `: `);
                if (tempLeaderboard[i].username != null) leaderboardValues.push(number.format(tempLeaderboard[i].money) + `$`);
                if (i == count - 1) break;
            }
            const embed = new Discord.MessageEmbed()
                .setColor(0x5886ce)
                .setTitle(`Leaderboard`)
                .addField(`Top ${i+1} Ballers`, leaderboardNames, true)
                .addField(`\u200b`, leaderboardValues, true)
            message.channel.send(embed);
            return;
    }
}

        //if(message.author.presence.clientStatus.desktop != null) {
        //    message.channel.send("desktop");
        //}
        //if(message.author.presence.clientStatus.mobile != null) {
        //    message.channel.send("mobile");
        //}
        //message.channel.send(Object.keys(message.author.presence.clientStatus));
