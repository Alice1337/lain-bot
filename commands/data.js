module.exports = {
    name: "data",
    cooldown: 0,
	run (client, message, args, cooldownTime) {
        //Constants
        const Discord = require('discord.js');
        const Canvas = require('canvas');
        const number = Intl.NumberFormat();
        if (message.channel.type != 'dm') SID = message.guild.id;
        if (message.channel.type == 'dm') SID = 'dm';
        if (message.mentions.members.first() != null) var user = message.mentions.members.first().user;
        if (message.mentions.members.first() == null) var user = message.author;
        if (message.mentions.members.first() != null) var username = message.mentions.members.first().user.username;
        if (message.mentions.members.first() == null) var username = message.author.username;

        //Variables
        var option = args[0];
        if (option != null) option = option.toLowerCase();
        var entry = args[1];
        let lbEntry = args[1];
        var value = args[2];
        var getEntry = client.db.getEco(SID, user, entry);
        var tempLeaderboard = client.db.lbEco(SID, entry);
        var leaderboardNames = [];
        var leaderboardValues = [];
        if ((value == null || isNaN(value)) && option == 'lb') entry = 5;


        //get
        if (option == 'get') {
            if (getEntry == null) return message.reply('Database entry `' + entry + '` does not exist');
            if ((entry.startsWith('lotto') || entry.startsWith('blackjack')) && message.author.id !== client.config.privileges.adminID) return message.reply('\nYou have no permission to view this database entry')
            message.channel.send('\nDatabase entry `' + entry + '` for `' + username +  '` is `' + getEntry + '`');
            return;
        }


        //leaderboard
        if (option == 'lb') {
            if (getEntry == null) return message.reply('Database entry `' + lbEntry + '` does not exist');
            if (value == null) {            
                for(i = 0; i < tempLeaderboard.length; i++) {
                    if (tempLeaderboard[i].username != null) leaderboardNames.push(`#${i+1} ` + tempLeaderboard[i].username + `: `);
                    if (tempLeaderboard[i].username != null) leaderboardValues.push(number.format(tempLeaderboard[i][lbEntry]));
                    if (i == 4) break;
                }
                const embed = new Discord.MessageEmbed()
                    .setColor(0x5886ce)
                    .setTitle(`Leaderboard`)
                    .addField(`Top 5`, leaderboardNames, true)
                    .addField(`\u200b`, leaderboardValues, true)
                message.channel.send(embed);
                return;
            }
            if (isNaN(value)) return message.channel.send(`${value} is not a number`);

            if (value != null) {
                var tempLeaderboard = client.db.lbEco(SID, entry);
                for(i = 0; i < tempLeaderboard.length; i++) {
                    if (tempLeaderboard[i].username != null) leaderboardNames.push(`#${i+1} ` + tempLeaderboard[i].username + `: `);
                    if (tempLeaderboard[i].username != null) leaderboardValues.push(number.format(tempLeaderboard[i][lbEntry]));
                    if (i == value - 1) break;
                }
                const embed = new Discord.MessageEmbed()
                    .setColor(0x5886ce)
                    .setTitle(`Leaderboard`)
                    .addField(`Top ${i}`, leaderboardNames, true)
                    .addField(`\u200b`, leaderboardValues, true)
                message.channel.send(embed);
                return;
            }
        }


        //check privilege
        if (option == null) return message.reply(`\nUse ` + '`' + `data get` + '`' + ` to get any database entry\nUse ` + '`' + `data lb` + '`' + ` to create any leaderboard`);
        if (message.author.id !== client.config.privileges.adminID) return message.reply(`\nYou have no permission to modify database entrys\nUse ` + '`' + `data get` + '`' + ` to get any database entry\nUse ` + '`' + `data lb` + '`' + ` to create any leaderboard`);


        //privileged
        if (message.author.id === client.config.privileges.adminID) {
            var getOldEntry = getEntry;
            //set
            if (option == 'set') {
                if (getEntry == null) return message.reply('Database entry `' + entry + '` does not exist');
                if (value == null) return message.reply(`no value has been provided`);
                client.db.setEco(SID, user, `${entry}`, `${value}`);
                getEntry = client.db.getEco(SID, user, `${entry}`);
                message.channel.send('\nOld database entry `' + entry + '` `' + getOldEntry + '`\nNew database entry `' + entry + '` `' + getEntry + '`');
                return;
            }

            //mk
            if (option == 'mk') {
                if (entry == null) return message.reply(`no entry has been provided`);
                if (option == null) return message.reply(`no value has been provided`);
                if (value == null) value = 0;
                client.db.setEco(SID, user, `${entry}`, `${value}`);
                getEntry = client.db.getEco(SID, user, `${entry}`);
                message.channel.send('\nNew database entry `' + entry + '` `' + getEntry + '` has been created');
                return;
            }

            //add
            if (option == 'add') {
                if (getEntry == null) return message.reply('Database entry `' + entry + '` does not exist');
                if (value == null) return message.reply(`no value has been provided`);
                if (isNaN(value)) return message.reply(`${value} is not a number`);
                if (isNaN(getEntry)) return message.reply('Database entry `' + entry + '` is not a number');
                client.db.addEco(SID, user, `${entry}`, `${value}`);
                getEntry = client.db.getEco(SID, user, `${entry}`);
                message.channel.send('\nOld database entry `' + entry + '` `' + getOldEntry + '`\nNew database entry `' + entry + '` `' + getEntry + '`');
                return;
            }
            
            //sub
            if (option == 'sub') {
                if (getEntry == null) return message.reply('Database entry `' + entry + '` does not exist');
                if (value == null) return message.reply(`no value has been provided`);
                if (isNaN(value)) return message.reply(`${value} is not a number`);
                if (isNaN(getEntry)) return message.reply('Database entry `' + entry + '` is not a number');
                client.db.subEco(SID, user, `${entry}`, `${value}`);
                getEntry = client.db.getEco(SID, user, `${entry}`);
                message.channel.send('\nOld database entry `' + entry + '` `' + getOldEntry + '`\nNew database entry `' + entry + '` `' + getEntry + '`');
                return;
            }
        }
    }
}

/*
Hex colors
    Red     0xa82418
    Black   0x141414
    Green   0x719b1f
    Blue    0x5886ce
    Yellow  0xc99400
*/