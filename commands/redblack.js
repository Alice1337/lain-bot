module.exports = {
    name: "redblack",
    cooldown: 3,
    async run (client, message, args, cooldownTime) {
        if (cooldownTime != 0) return message.reply(`\nplease wait ${cooldownTime} seconds to play again`);
        const Discord = require('discord.js');
        const Canvas = require('canvas');
        var number = Intl.NumberFormat();
        if (message.channel.type != 'dm') SID = message.guild.id;
        if (message.channel.type == 'dm') SID = 'dm';
        var cards = ["Ad", "2d", "3d", "4d", "5d", "6d", "7d", "8d", "9d", "10d", "Jd", "Qd", "Kd", "Ah", "2h", "3h", "4h", "5h", "6h", "7h", "8h", "9h", "10h", "Jh", "Qh", "Kh", "Ac", "2c", "3c", "4c", "5c", "6c", "7c", "8c", "9c", "10c", "Jc", "Qc", "Kc", "As", "2s", "3s", "4s", "5s", "6s", "7s", "8s", "9s", "10s", "Js", "Qs", "Ks"];
        var blackCards = ["Ac", "2c", "3c", "4c", "5c", "6c", "7c", "8c", "9c", "10c", "Jc", "Qc", "Kc", "As", "2s", "3s", "4s", "5s", "6s", "7s", "8s", "9s", "10s", "Js", "Qs", "Ks"];
        var redCards = ["Ad", "2d", "3d", "4d", "5d", "6d", "7d", "8d", "9d", "10d", "Jd", "Qd", "Kd", "Ah", "2h", "3h", "4h", "5h", "6h", "7h", "8h", "9h", "10h", "Jh", "Qh", "Kh"];
        
        var color = args[0];
        if (color != null) color = color.toLowerCase();
        var money = args[1];
        var lowerLimit = client.config.lowerLimit;
        var balance = client.db.getEco(SID, message.author, `money`);

        if ((color == null) || (money == null)) return message.reply(`\nDo 'redblack red/black 500' to play`);
        if (isNaN(money)) return message.reply(`\n${money} is not a valid number`);
        if (money < 0) return message.reply(`\nthat doesn't seem to be right`);
        if (money < lowerLimit) return message.reply(`\nYou need at least ${lowerLimit}$ to play`);
        if (balance < money) return message.reply(`\nYou don't have enough Money to play\ncurrent balance: ${number.format(balance)}$`);

        //veto function
        //set name of current game
        client.db.setEco(SID, message.author, `veto_game`, client.commands.get(message.content.slice(prefix.length).trim().split(/ +/g).shift().toLowerCase()).name);
        //set win/loss
        //set +/- depending on the outcome of the game
        function veto_stats(win, status) {
            //veto
            client.db.setEco(SID, message.author, `veto_money`, win);
            client.db.setEco(SID, message.author, `veto_status`, status);
        }
        

        //Game Logic
        //Generate Cards
        var cards = [cards[Math.floor(Math.random() * cards.length)], cards[Math.floor(Math.random() * cards.length)], cards[Math.floor(Math.random() * cards.length)]];

        //Player chose black cards
        var black = 0;
        if (blackCards.includes(cards[0]) == true) black = black + 1;
        if (blackCards.includes(cards[1]) == true) black = black + 1;
        if (blackCards.includes(cards[2]) == true) black = black + 1;

        if (color == 'black') {
            if (black == 1 || black == 0) {
                color = 0xa82418;
                var post = `You lost ${number.format(money)}$`;
                client.db.addEco(SID, message.author, `xp`, client.db.prestigeXP(SID, message.author, 5));
                client.db.subEco(SID, message.author, `money`, money);
                //veto
                veto_stats(money, '-');
            }
            if (black == 2) {
                color = 0x141414;
                money = client.db.prestigeMoney(SID, message.author, money);
                var post = `You Won ${number.format(money)}$`;
                client.db.addEco(SID, message.author, `xp`, client.db.prestigeXP(SID, message.author, 10));
                client.db.addEco(SID, message.author, `money`, money);
                //veto
                veto_stats(money, '+');
            }
            if (black == 3) {
                color = 0x141414;
                var win = money * 2;
                win = client.db.prestigeMoney(SID, message.author, win);
                var post = `You Won ${number.format(win)}$`;
                client.db.addEco(SID, message.author, `xp`, 20);
                client.db.addEco(SID, message.author, `money`, win);
                //veto
                veto_stats(win, '+');
            }

        }

        //Player chose red cards
        var red = 0;
        if (redCards.includes(cards[0]) == true) red = red +1;
        if (redCards.includes(cards[1]) == true) red = red +1;
        if (redCards.includes(cards[2]) == true) red = red +1;

        if (color == 'red') {
            if (red == 1 || red == 0) {
                color = 0x141414;
                var post = `You lost ${number.format(money)}$`;
                client.db.addEco(SID, message.author, `xp`, client.db.prestigeXP(SID, message.author, 5));
                client.db.subEco(SID, message.author, `money`, money);
                //veto
                veto_stats(money, '-');
            }
            if (red == 2) {
                color = 0xa82418;
                money = client.db.prestigeMoney(SID, message.author, money);
                var post = `You Won ${number.format(money)}$`;
                client.db.addEco(SID, message.author, `xp`, client.db.prestigeXP(SID, message.author, 10));
                client.db.addEco(SID, message.author, `money`, money);
                //veto
                veto_stats(money, '+');
            }
            if (red == 3) {
                color = 0xa82418;
                var win = money * 2;
                win = client.db.prestigeMoney(SID, message.author, win);
                var post = `You Won ${number.format(win)}$`;
                client.db.addEco(SID, message.author, `xp`, 20);
                client.db.addEco(SID, message.author, `money`, win);
                //veto
                veto_stats(win, '+');
            }

        }


        //Canvas
        //Base Canvas and Background
        var img = './assets/background.png';
        const canvas = Canvas.createCanvas(512, 512);
        const ctx = canvas.getContext('2d');
        const background = await Canvas.loadImage(img);
        ctx.drawImage(background, 0, 0, canvas.width, canvas.height);
        //Game Text
        ctx.textAlign = 'center';
        ctx.font = `600 59px sans-serif`;
        ctx.strokeStyle = '#40195C';
        ctx.strokeText(`lainchan`, 256, 425);
        ctx.fillStyle = '#40195C';
        ctx.fillText(`lainchan`, 256, 425);
        ctx.font = `600 25px sans-serif`;
        ctx.strokeStyle = '#393939';
        ctx.strokeText(`R  E  D  B  L  A  C  K`, 256, 460);
        //ctx.strokeText(`C     A     R     D     S`, 256, 460);
        ctx.fillStyle = '#787878';
        ctx.fillText(`R  E  D  B  L  A  C  K`, 256, 460);
        //ctx.fillText(`C     A     R     D     S`, 256, 460);
        ctx.textAlign = 'right';
        //Render drawn Cards
        var card1 = await Canvas.loadImage(`./assets/cards/${cards[0]}.png`);
        ctx.drawImage(card1, 132, 120);
        var card2 = await Canvas.loadImage(`./assets/cards/${cards[1]}.png`);
        ctx.drawImage(card2, 194, 120);
        var card3 = await Canvas.loadImage(`./assets/cards/${cards[2]}.png`);
        ctx.drawImage(card3, 256, 120);0
        //Create Canvas
        const attachment = new Discord.MessageAttachment(canvas.toBuffer(), `cards.png`);

        //Embed
        var embedPost = new Discord.MessageEmbed()
            .setColor(color)
            .setAuthor(`${message.author.username} played redblack`, message.author.avatarURL())
            .setDescription(post)
            .attachFiles(attachment)
            .setImage('attachment://cards.png')
        message.channel.send(embedPost);

    }
}