exports.run = (client, message, args) => {
    if (message.mentions.members != null) var name = message.mentions.members.first();
    var number = Intl.NumberFormat();
    if (message.channel.type != 'dm') SID = message.guild.id;
    if (message.channel.type == 'dm') SID = 'dm';


    if ((name == null) && (client.db.getEco(SID, message.author, `money`) != "0")) {
        message.reply(`\nYou have ${number.format(client.db.getEco(SID, message.author, `money`))}$, spend it wisely\n` + '`' + client.db.getEco(SID, message.author, `money`) + '`');
        return;
    }

    if ((name == null) && (client.db.getEco(SID, message.author, `money`) == "0")) {
        message.reply(`\nYou have ${number.format(client.db.getEco(SID, message.author, `money`))}$\nUse '${client.config.botSettings.prefix}daily' or '${client.config.botSettings.prefix}weekly' to get more`);
        return;
    }
    
    if (name != null) {
        let name = message.mentions.members.first().user.username;
        let user = message.mentions.members.first();
        message.channel.send(`${name} has ${number.format(client.db.getEco(SID, user, `money`))}$\n` + '`' + client.db.getEco(SID, user, `money`) + '`');
        return;
    }
}