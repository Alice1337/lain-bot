module.exports = {
    name: "highlow",
    cooldown: 3,
    async run (client, message, args, cooldownTime) {
        if (cooldownTime != 0) return message.reply(`\nplease wait ${cooldownTime} seconds to play again`);
        const Discord = require('discord.js');
        const Canvas = require('canvas');
        const number = Intl.NumberFormat();
        if (message.channel.type != 'dm') SID = message.guild.id;
        if (message.channel.type == 'dm') SID = 'dm';
        var cards = ["Ad", "2d", "3d", "4d", "5d", "6d", "7d", "8d", "Ah", "2h", "3h", "4h", "5h", "6h", "7h", "8h", "Ac", "2c", "3c", "4c", "5c", "6c", "7c", "8c", "As", "2s", "3s", "4s", "5s", "6s", "7s", "8s"];
        var highCards = ["5d", "6d", "7d", "8d", "5h", "6h", "7h", "8h", "5c", "6c", "7c", "8c", "5s", "6s", "7s", "8s"];
        var lowCards = ["Ad", "2d", "3d", "4d", "Ah", "2h", "3h", "4h", "Ac", "2c", "3c", "4c", "As", "2s", "3s", "4s"];
        var balance = client.db.getEco(SID, message.author, `money`);
        var option = args[0];
        if (option != null) option = option.toLowerCase();
        var money = args[1];
        var lowerLimit = client.config.lowerLimit;

        
        if (option != 'high' && option != 'low') return message.reply(`\nPlay with 'highlow high/low money'`);
        if (isNaN(money)) return message.reply(`\n${money} is not a valid number`);
        if (money < 0) return message.reply(`\nthat doesn't seem to be right`);
        if (money < lowerLimit) return message.reply(`\nYou need at least ${lowerLimit}$ to play`);
        if (balance < money) return message.reply(`\nYou don't have enough Money to play\ncurrent balance: ${number.format(balance)}$`);


        //veto function
        //set name of current game
        client.db.setEco(SID, message.author, `veto_game`, client.commands.get(message.content.slice(prefix.length).trim().split(/ +/g).shift().toLowerCase()).name);
        //set win/loss
        //set +/- depending on the outcome of the game
        function veto_stats(win, status) {
            //veto
            client.db.setEco(SID, message.author, `veto_money`, win);
            client.db.setEco(SID, message.author, `veto_status`, status);
        }
        
        
        //Generate Card
        var card = cards[Math.floor(Math.random() * cards.length)];
        

        //Gamelogic
        if (option == 'high') {
            //high win
            if (highCards.includes(card) == true) {
                color = '0x719b1f';
                money = client.db.prestigeMoney(SID, message.author, money);
                var post = `You Won ${number.format(money)}$`;
                client.db.addEco(SID, message.author, `xp`, client.db.prestigeXP(SID, message.author, 10));
                client.db.addEco(SID, message.author, `money`, money);
                //veto
                veto_stats(money, '+');
            }
            //high lost
            if (highCards.includes(card) == false) {
                color = '0xa82418';
                var post = `You lost ${number.format(money)}$`;
                client.db.addEco(SID, message.author, `xp`, client.db.prestigeXP(SID, message.author, 5));
                client.db.subEco(SID, message.author, `money`, money);
                //veto
                veto_stats(money, '-');
            }
        }

        if (option == 'low') {
            //high win
            if (lowCards.includes(card) == true) {
                color = '0x719b1f';
                money = client.db.prestigeMoney(SID, message.author, money);
                var post = `You Won ${number.format(money)}$`;
                client.db.addEco(SID, message.author, `xp`, client.db.prestigeXP(SID, message.author, 10));
                client.db.addEco(SID, message.author, `money`, money);
                //veto
                veto_stats(money, '+');
            }
            //high lost
            if (lowCards.includes(card) == false) {
                color = '0xa82418';
                var post = `You lost ${number.format(money)}$`;
                client.db.addEco(SID, message.author, `xp`, client.db.prestigeXP(SID, message.author, 5));
                client.db.subEco(SID, message.author, `money`, money);
                //veto
                veto_stats(money, '-');
            }
        }


        //Canvas
        //Base Canvas and Background
        var img = './assets/background.png';
        const canvas = Canvas.createCanvas(512, 512);
        const ctx = canvas.getContext('2d');
        const background = await Canvas.loadImage(img);
        ctx.drawImage(background, 0, 0, canvas.width, canvas.height);
        var fontSizeThin = 600;
        var fontSizeBold = 600;
        //Game Text
        ctx.textAlign = 'center';
        ctx.font = `${fontSizeBold} 58px sans-serif`;
        ctx.strokeStyle = '#40195C';
        ctx.strokeText(`lainchan`, 256, 425);
        ctx.fillStyle = '#40195C';
        ctx.fillText(`lainchan`, 256, 425);
        ctx.font = `${fontSizeThin} 23px sans-serif`;
        ctx.strokeStyle = '#393939';
        ctx.strokeText(`H   I   G   H   L   O   W`, 256, 460);
        ctx.fillStyle = '#787878';
        ctx.fillText(`H   I   G   H   L   O   W`, 256, 460);
        ctx.textAlign = 'right';
        //Draw Player Cards
        var card = await Canvas.loadImage(`./assets/cards/${card}.png`);
        //const card = await Canvas.loadImage(`./assets/cards/backside.png`);
        ctx.drawImage(card, 194, 120);
        //Create Canvas
        const attachment = new Discord.MessageAttachment(canvas.toBuffer(), `cards.png`);


        //Embed
        var embedPost = new Discord.MessageEmbed()
            .setColor(color)
            .setAuthor(`${message.author.username} played highlow`, message.author.avatarURL())
            .setDescription(post)
            .attachFiles(attachment)
            .setImage('attachment://cards.png')
        message.channel.send(embedPost);
    }
}