exports.run = (client, message, args) => {
    //check privileges
    var priv = client.config.privileges;
    if (message.author.id !== priv.adminID) return message.channel.send(`You have no permission to reload me`);


    if (!args || args.lenght < 1) return message.reply("please specify what I shall reload.");
    const commandName = args[0].toLowerCase();

    if (!client.commands.has(commandName)) return message.channel.send(`I can't find '${commandName}.js'.\n'list' to show all Commands\n'list owner' for ownerCommands`);
    delete require.cache[require.resolve(`./${commandName}.js`)];
    client.commands.delete(commandName);
    const props = require(`./${commandName}.js`);
    client.commands.set(commandName, props);
    message.channel.send(`I've reloaded '${commandName}.js' for you.`);
    return;
}