exports.run = (client, message, args) => {
    //check privileges
    var priv = client.config.privileges;
    if (message.author.id !== priv.adminID) return message.channel.send(`You have no permission to do that`);


    function clean(text) {
        if (typeof(text) === "string")
            return text.replace(/`/g, "`" + String.fromCharCode(8203)).replace(/@/g, "@" + String.fromCharCode(8203));
        else
            return text;
    }

    var args = message.content.split(" ").slice(1);
    try {
        const code = args.join(" ");
        let evaled =eval(code);

        if (typeof evealed !== "string")
            evaled = require("util").inspect(evaled);

        message.channel.send(clean(evaled), {code:"xl"});
    } catch (err) {
        message.channel.send(`\`ERROR\` \`\`\`xl\n${clean(err)}\n\`\`\``);
    }
}