module.exports = {
    name: "template",
    cooldown: 0,
	async run (client, message, args, cooldownTime) {
        return;
        //Cooldown
        if (cooldownTime != 0) return message.reply(`\nplease wait ${cooldownTime} seconds to play again`);

        //Constants
        const Discord = require('discord.js');
        const Canvas = require('canvas');
        const number = Intl.NumberFormat();
        if (message.channel.type != 'dm') SID = message.guild.id;
        if (message.channel.type == 'dm') SID = 'dm';
        if (message.mentions.members != null) var user = message.mentions.members.first();

        //check privileges
        var priv = client.config.privileges;
        if (message.author.id != priv.adminID) message.channel.send(`You have admin privileges`);
        if (message.author.id == priv.adminID) message.channel.send(`You have the default privileges`);

        //Variables
        var money = args[0];
        var option = args[1];
        if (option != null) option = option.toLowerCase();
        var lowerLimit = client.config.lowerLimit;
        var balance = client.db.getEco(SID, message.author, `money`);
        var gameName = client.commands.get(message.content.slice(prefix.length).trim().split(/ +/g).shift().toLowerCase()).name;

        //Check money and balance
        if (isNaN(money)) return message.reply("\nYou can't play for free\nDo `" + gameName + " 500` to start playing");
        if (money < 0) return message.reply(`\nthat doesn't seem to be right`);
        if (money < lowerLimit) return message.reply(`\nYou need at least ${lowerLimit}$ to play`);
        if (balance < money) return message.reply(`\nYou don't have enough Money to play\ncurrent balance: ${number.format(balance)}$`);

        //veto function
        client.db.setEco(SID, message.author, `veto_game`, gameName);
        //set the money won/lost with win
        //set status + if the user has won and - if the user has lost
        function veto_stats(win, status) {
            client.db.setEco(SID, message.author, `veto_money`, win);
            client.db.setEco(SID, message.author, `veto_status`, status);
        }
        //sleep
        async function sleep(ms) {
            return new Promise(resolve => setTimeout(resolve, ms));
        }

        //code goes here
        
    }
}

/*
Hex colors
    Red     0xa82418
    Black   0x141414
    Green   0x719b1f
    Blue    0x5886ce
    Purple  0x9059cf
    Yellow  0xc99400

Prestige Win
    win = client.db.prestigeMoney(SID, message.author, win);
Prestige XP
    client.db.addEco(SID, message.author, `xp`, client.db.prestigeXP(SID, message.author, 10));
*/