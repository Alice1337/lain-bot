module.exports = {
    name: "coinflip",
    cooldown: 3,
    async run (client, message, args, cooldownTime) {
        if (cooldownTime == 0) {
            const Discord = require('discord.js');
            var number = Intl.NumberFormat();
            if (message.channel.type != 'dm') SID = message.guild.id;
            if (message.channel.type == 'dm') SID = 'dm';
            var coinflip = ["coinflip_heads.png", "coinflip_tails.png"];
            var balance = client.db.getEco(SID, message.author, `money`);
            var lowerLimit = client.config.lowerLimit;
            var money = args[0];
            var roll = args[0];
            var enemyName = args[1];
            if (roll != null) roll = roll.toLowerCase();
            

            if (message.mentions.members == null) return message.reply(`\nYou can't play that with yourself\nDo '${client.config.botSettings.prefix}coinflip 500 @user' or '${client.config.botSettings.prefix}help coinflip'`);
            if (message.mentions.members != null) var user = message.mentions.members.first();
            if (user != null) user = message.mentions.members.first().user;
            if (user != null) var name = message.mentions.members.first().user.username;
            if (user == null) return message.channel.send(`Choose someone to play against\ncoinflip heads/tails @user\n${client.config.botSettings.prefix}help coinflip`);
            //if (isNaN(money)) return message.reply(`\n${money} is not a valid number`);
            if (money < 0) return message.reply(`\nthat doesn't seem to be right`);
            if (money < lowerLimit) return message.reply(`\nYou need at least ${lowerLimit}$ to play`);
            if (balance < money) return message.reply(`\nYou don't have that much Money`);

            var user = message.mentions.members.first().user


            //veto function
            //set name of current game
            client.db.setEco(SID, message.author, `veto_game`, client.commands.get(message.content.slice(prefix.length).trim().split(/ +/g).shift().toLowerCase()).name);
            client.db.setEco(SID, user, `veto_game`, client.commands.get(message.content.slice(prefix.length).trim().split(/ +/g).shift().toLowerCase()).name);
            //set win/loss
            //set +/- depending on the outcome of the game
            function veto_stats(win, status) {
                //veto
                client.db.setEco(SID, message.author, `veto_money`, win);
                client.db.setEco(SID, message.author, `veto_status`, status);
            }

            //coinflip battle initiation
            var enemyMoney = client.db.getEco(SID, user, `money`);
            if (enemyMoney >= money) {
                client.db.setEco(SID, user, `coinflip_battle`, message.author.id);
                client.db.setEco(SID, user, `coinflip_money`, money);
                message.channel.send(`Hey ${enemyName}, ${message.author.username} challanged you to a ${number.format(money)}$ coinflip battle`);
                return;
            } else
            if (enemyMoney < money) return message.reply(`\n${name} has only ${number.format(enemyMoney)}$ left\nNo Battle will be initiated`);


            //coinflip battle accept
            var coinflipMoney = client.db.getEco(SID, message.author, `coinflip_money`);
            var coinflipUser = client.db.getEco(SID, message.author, `coinflip_battle`);
            if (roll == "heads" || roll == "tails" && coinflipUser != 0) {
                if (coinflipUser == user && balance >= coinflipMoney) {
                    if (client.db.getEco(SID, user, `money`) < coinflipMoney || client.db.getEco(SID, message.author, `money`) < coinflipMoney) return message.reply(`Not enough money`);
                    var coinflip = coinflip[Math.floor(Math.random() * coinflip.length)];
                    client.db.setEco(SID, message.author, `coinflip_battle`, 0);
                    client.db.setEco(SID, message.author, `coinflip_money`, 0);
                    if (coinflip == `coinflip_${roll}.png`) {
                        client.db.addEco(SID, message.author, `money`, coinflipMoney);
                        client.db.subEco(SID, user, `money`, coinflipMoney);

                        client.db.addEco(SID, message.author, `xp`, client.db.prestigeXP(SID, message.author, 10));
                        client.db.addEco(SID, user, `xp`, client.db.prestigeXP(SID, user, 5));

                        var embedPost = new Discord.MessageEmbed()
                            .setColor(0x5886ce)
                            .setAuthor(`${message.author.username} played a coinflip battle with ${name}`, message.author.avatarURL())
                            .addField(`${message.author.username} Won ${number.format(coinflipMoney)}$`, `${name} Lost ${number.format(coinflipMoney)}$`)
                            .attachFiles(`./assets/coinflip/${coinflip}`)
                            .setImage(`attachment://${coinflip}`)
                        message.channel.send(embedPost).catch(console.error);
                        return;
                    } else
                        client.db.subEco(SID, message.author, `money`, coinflipMoney);
                        client.db.addEco(SID, user, `money`, coinflipMoney);

                        client.db.addEco(SID, message.author, `xp`, client.db.prestigeXP(SID, message.author, 5));
                        client.db.addEco(SID, user, `xp`, client.db.prestigeXP(SID, user, 10));

                        var embedPost = new Discord.MessageEmbed()
                            .setColor(0x5886ce)
                            .setAuthor(`${message.author.username} played a coinflip battle with ${name}`, message.author.avatarURL())
                            .addField(`${name} Won ${number.format(coinflipMoney)}$`, `${message.author.username} Lost ${number.format(coinflipMoney)}$`)
                            .attachFiles(`./assets/coinflip/${coinflip}`)
                            .setImage(`attachment://${coinflip}`)
                        message.channel.send(embedPost).catch(console.error);
                        return;                        
                } else
                if (balance < coinflipMoney) return message.reply(`\nYour balance is below ${number.format(coinflipMoney)}$\nNo Battle will be started`);
                if (coinflipUser != user) return message.reply(`\nYou'r no longer challanged by this user`);
            } else
            if (roll == 'retract') {
                client.db.setEco(SID, message.author, `coinflip_battle`, 0);
                client.db.setEco(SID, message.author, `coinflip_money`, 0);
                message.reply(`\nYou've retracted your cinflip battle`)
            }


        } return message.reply(`\nplease wait ${cooldownTime} seconds to play again`);
    }
}
