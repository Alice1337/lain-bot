module.exports = {
    name: "weekly",
    cooldown: 604800,
	run (client, message, args, cooldownTime) {
        var number = Intl.NumberFormat();
        if (message.channel.type != 'dm') SID = message.guild.id;
        if (message.channel.type == 'dm') SID = 'dm';
        var weekly = 50000;


        if (cooldownTime == 0) {
            client.db.addEco(SID, message.author, `money`, weekly);
            client.db.addEco(SID, message.author, `xp`, client.db.prestigeXP(SID, message.author, 20));
            var weeklyFormat = number.format(weekly);
            message.reply(`\nYou claimed your weekly ${weeklyFormat}$`);
            return;
        }

        if (cooldownTime <= 60) return message.reply(`\nYou already claimed your weekly\nplease wait ${cooldownTime} seconds to reclaim`);
        
        if (cooldownTime <= 3600) {
            cooldownTime = cooldownTime / 60;
            message.reply(`\nYou already claimed your weekly\nplease wait ${cooldownTime.toFixed(0)} minutes to reclaim`);
            return;
        }

        if (cooldownTime <= 86400) {
            cooldownTime = cooldownTime / 60 / 60;
            message.reply(`\nYou already claimed your weekly\nplease wait ${cooldownTime.toFixed(1)} hours to reclaim`);
            return;
        }

        if (cooldownTime > 86400) {
            cooldownTime = cooldownTime / 60 / 60 / 24;
            message.reply(`\nYou already claimed your weekly\nplease wait ${cooldownTime.toFixed(1)} days to reclaim`);
            return;
        }
    }
}