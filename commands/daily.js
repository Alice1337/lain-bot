module.exports = {
    name: "daily",
    cooldown: 86400,
	run (client, message, args, cooldownTime) {
        var number = Intl.NumberFormat();
        if (message.channel.type != 'dm') SID = message.guild.id;
        if (message.channel.type == 'dm') SID = 'dm';
        var daily = 5000;


        if (cooldownTime == 0) {
            client.db.addEco(SID, message.author, `money`, daily);
            client.db.addEco(SID, message.author, `xp`, client.db.prestigeXP(SID, message.author, 10));
            message.reply(`\nYou claimed your daily ${number.format(daily)}$`);
            return;
        }

        if (cooldownTime <= 60) return message.reply(`You already claimed your daily\nplease wait ${cooldownTime} seconds to reclaim`);
        
        if (cooldownTime <= 3600) {
            cooldownTime = cooldownTime / 60;
            message.reply(`\nYou already claimed your daily\nplease wait ${cooldownTime.toFixed(0)} minutes to reclaim`);
            return;
        }

        if (cooldownTime > 3600) {
            cooldownTime = cooldownTime / 60 / 60;
            message.reply(`\nYou already claimed your daily\nplease wait ${cooldownTime.toFixed(1)} hours to reclaim`);
            return;
        }
    }
}