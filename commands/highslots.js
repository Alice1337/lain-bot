module.exports = {
    name: "highslots",
    cooldown: 2,
    run (client, message, args, cooldownTime) {
        if (cooldownTime == 0) {
            const Discord = require('discord.js');
            var number = Intl.NumberFormat();
            if (message.channel.type != 'dm') SID = message.guild.id;
            if (message.channel.type == 'dm') SID = 'dm';
            let symbol = [":apple:", ":lemon:", ":watermelon:", ":grapes:", ":cherries:", ":star:"];// ":tangerine:", ":blueberries:",
            //let symbol = [":apple:"];//, ":lemon:", ":watermelon:", ":grapes:", ":cherries:", ":star:"];// ":tangerine:", ":blueberries:",
            let money = args[0]
            let count = args[1];
            let maxSlots = client.config.maxHighslots;
            var win = 0;
            var balance = client.db.getEco(SID, message.author, `money`);

            var oneLine = 5;
            var twoLines = 10;
            var bigWin = 15;
            var jackpot = 40;
            var vollbild = 80;

            var upperLimit = 500000;
            var lowerLimit = client.config.lowerLimit;

            if (isNaN(money)) return message.reply(`\n${money} is not a valid number`);
            if (money < 0) return message.reply(`\nthat doesn't seem to be right`);
            if (money < lowerLimit) return message.reply(`\nYou need at least ${lowerLimit}$ to play`);
        

            //veto function
            //set name of current game
            client.db.setEco(SID, message.author, `veto_game`, client.commands.get(message.content.slice(prefix.length).trim().split(/ +/g).shift().toLowerCase()).name);
            //set win/loss
            //set +/- depending on the outcome of the game
            function veto_stats(win, status) {
                //veto
                client.db.setEco(SID, message.author, `veto_money`, win);
                client.db.setEco(SID, message.author, `veto_status`, status);
            }
        

            //Single Game
            if ((count == null) && (money <= upperLimit) && (money >= lowerLimit) && (balance >= money)) {
                //var win = 0;
                var S = [];
                for (i = 0; i < 9; i++) {
                    S[i] = symbol[Math.floor(Math.random() * symbol.length)];
                }
    
                function embedPost(color, postMessage, win) {
                    if (win.startsWith('-')) client.db.addEco(SID, message.author, `xp`, client.db.prestigeXP(SID, message.author, 5));
                    if (win.startsWith('+')) client.db.addEco(SID, message.author, `xp`, client.db.prestigeXP(SID, message.author, 10));
                    const embedPost = new Discord.MessageEmbed()
                        .setColor(color)
                        .setAuthor(`${message.author.username} played slots`, message.author.avatarURL())
                        .setTitle(`${S[0]} ${S[1]} ${S[2]}\n${S[3]} ${S[4]} ${S[5]}\n${S[6]} ${S[7]} ${S[8]}`)
                        .addField(postMessage, `${number.format(win)}$`)
                    message.channel.send(embedPost);
                    return;
                }

                //Vollbild
                if (((S[0] == S[1]) && (S[1] == S[2])) && ((S[3] == S[4]) && (S[4] == S[5])) && ((S[6] == S[7]) && (S[7] == S[8]))) {
                    win = money * vollbild;
                    win = client.db.prestigeMoney(SID, message.author, win);
                    client.db.addEco(SID, message.author, `money`, win);
                    //veto
                    veto_stats(win, '+');
                    embedPost (0xc99400, "Vollbild!", `+${win}`);
                    return;
                } else
                //Mid line Sterne
                if ((S[3] == ":star:") && (S[4] == ":star:") && (S[5] == ":star:")) {
                    win = money * jackpot;
                    win = client.db.prestigeMoney(SID, message.author, win);
                    client.db.addEco(SID, message.author, `money`, win);
                    //veto
                    veto_stats(win, '+');
                    embedPost (0xc99400, "Jackpot!", `+${win}`);
                    return;
                } else
                //Mid Line
                if ((S[3] == S[4]) && (S[4] == S[5])) {
                    win = money * bigWin;
                    win = client.db.prestigeMoney(SID, message.author, win);
                    client.db.addEco(SID, message.author, `money`, win);
                    //veto
                    veto_stats(win, '+');
                    embedPost (0x719b1f, "Big Win!", `+${win}`);
                    return;
                } else
                //Upper and Lower Line
                if (((S[0] == S[1]) && (S[1] == S[2])) && ((S[6] == S[7]) && (S[7] == S[8]))) {
                    win = money * twoLines;
                    win = client.db.prestigeMoney(SID, message.author, win);
                    client.db.addEco(SID, message.author, `money`, win);
                    //veto
                    veto_stats(win, '+');
                    embedPost (0x5886ce, "Two Lines", `+${win}`);
                    return;
                } else
                //Upper or Lower Line
                if (((S[0] == S[1]) && (S[1] == S[2])) || ((S[6] == S[7]) && (S[7] == S[8]))) {
                    win = money * oneLine;
                    win = client.db.prestigeMoney(SID, message.author, win);
                    client.db.addEco(SID, message.author, `money`, win);
                    //veto
                    veto_stats(win, '+');
                    embedPost (0x5886ce, "One Line", `+${win}`);
                    return;
                } else
                //Nothing
                    win = money;
                    client.db.subEco(SID, message.author, `money`, win);
                    //veto
                    veto_stats(win, '-');
                    embedPost (0xa82418, "No luck this time", `-${win}`);
                    return;
            } else


            //Multiple Games
            if ((count > 0) && (money <= upperLimit) && (balance >= (count * money))) {
                if (money >= 1000 && !money.endsWith(000)) return message.reply(`\nYour amount must end with 000 when playing with more than 1.000$`)
                var winSum = 0;
                var winArray = [];
                countMoney = count * money;
                if ((balance >= countMoney) && (count <= maxSlots)) {
                    loop = 0;

                    function embedPostMulti(color, postMessage, winArray) {
                        const embedPostMulti = new Discord.MessageEmbed()
                            .setColor(color)
                            .setAuthor(`${message.author.username} played slots`, message.author.avatarURL())
                            .addField(`${winArray}`, postMessage)
                        message.channel.send(embedPostMulti);
                        return;
                    }

                    function calculate(win, prestigewin) {
                        winSum = winSum += parseInt(prestigewin);
                        client.db.addEco(SID, message.author, `money`, prestigewin);
                        if (win >= 1000000) {
                            win = win / 1000000;
                            winArray.push(`\n+${win}m$`);
                            x = 1;
                        }
                        if (win >= 1000 && x == 0) {
                            win = win / 1000;
                            winArray.push(`\n+${win}k$`);
                            x = 1;
                        }
                        if (win < 1000 && x == 0) winArray.push(`\n+${number.format(win)}$`);
                        client.db.addEco(SID, message.author, `xp`, client.db.prestigeXP(SID, message.author, 10));
                        roll = 1;
                        x = 0;
                    }

                    while (loop < count) {
                        //var win = 0;
                        var tempWin = 0;
                        var x = 0;
                        var roll = 0;
                        var S = [];
                        for (i = 0; i < 9; i++) {
                            S[i] = symbol[Math.floor(Math.random() * symbol.length)];
                        }

                        //Vollbild
                        if (((S[0] == S[1]) && (S[1] == S[2])) && ((S[3] == S[4]) && (S[4] == S[5])) && ((S[6] == S[7]) && (S[7] == S[8]))) {
                            win = money * vollbild;
                            prestigewin = client.db.prestigeMoney(SID, message.author, win);
                            calculate(win, prestigewin);
                            loop++;
                        } else
                        //Mid line Sterne
                        if ((S[3] == ":star:") && (S[4] == ":star:") && (S[5] == ":star:")) {
                            win = money * jackpot;
                            prestigewin = client.db.prestigeMoney(SID, message.author, win);
                            calculate(win, prestigewin);
                            loop++;
                        } else
                        //Mid Line
                        if ((S[3] == S[4]) && (S[4] == S[5])) {
                            win = money * bigWin;
                            prestigewin = client.db.prestigeMoney(SID, message.author, win);
                            calculate(win, prestigewin);
                            loop++;
                        } else
                        //Upper and Lower Line
                        if (((S[0] == S[1]) && (S[1] == S[2])) && ((S[6] == S[7]) && (S[7] == S[8]))) {
                            win = money * twoLines;
                            prestigewin = client.db.prestigeMoney(SID, message.author, win);
                            calculate(win, prestigewin);
                            loop++;
                        }  else
                        //Upper or Lower Line
                        if (((S[0] == S[1]) && (S[1] == S[2])) || ((S[6] == S[7]) && (S[7] == S[8]))) {
                            win = money * oneLine;
                            prestigewin = client.db.prestigeMoney(SID, message.author, win);
                            calculate(win, prestigewin);
                            loop++;
                        } else
                        if (roll = 1) {
                            win = money;
                            winSum = winSum -= win
                            client.db.subEco(SID, message.author, `money`, win);
                            if (win >= 1000000) {
                                win = win / 1000000;
                                winArray.push(`\n-${win}m$`);
                                x = 1;
                            }
                            if (win >= 1000 && x == 0) {
                                win = win / 1000;
                                winArray.push(`\n-${win}k$`);
                            }
                            if (win <= -1000 && money >= 1000) winArray.push(`\n-${number.format(win)}$`);
                            if (win > -1000 && money < 1000) winArray.push(`\n-${number.format(win)}$`);
                            x = 0;
                            roll = 1;
                            client.db.addEco(SID, message.author, `xp`, client.db.prestigeXP(SID, message.author, 5));
                            loop++
                        }
                    }
                    if (winSum > 0) {
                        postMessage = `You Won ${number.format(winSum)}$`;
                        //veto
                        veto_stats(winSum, '+');
                        color = 0x718f32;
                        embedPostMulti (color, postMessage, winArray);
                        return;
                    } else
                    if (winSum < 0) {
                        postMessage = `You lost ${number.format(winSum)}$`;
                        win = winSum - winSum - winSum;
                        //veto
                        veto_stats(win, '-');
                        color = 0xa82418;
                        embedPostMulti (color, postMessage, winArray);
                        return;
                    } else
                    if (winSum == 0) {
                        postMessage = `You Neither won nor lost`;
                        //veto
                        veto_stats(winSum, '+');
                        color = 0x5886ce;
                        embedPostMulti (color, postMessage, winArray);
                        return;
                    }
                    return;


                } else
                if (count > maxSlots) return message.reply(`\nYou can't play more than ${maxSlots} Slots`);
                return message.reply(`\nYou need at least ${number.format(countMoney)}$ to play ${count} Slots`);


            } else
            if (money > upperLimit) return message.reply(`\nYou can't bet more than ${number.format(upperLimit)}$`);
            if (money == null) return message.reply(`\nYou Can't play without money`);
            return message.reply(`\nYou don't have enough money\nYour balance is ${number.format(balance)}`);


        } return message.reply(`\nplease wait ${cooldownTime} seconds to play again`);
    }
}
