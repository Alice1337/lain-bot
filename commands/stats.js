module.exports = {
    name: "stats",
    cooldown: 0,
    async run (client, message, args, cooldownTime) {
        const Discord = require('discord.js');
        const Canvas = require('canvas');
        var number = Intl.NumberFormat();
        if (message.channel.type != 'dm') SID = message.guild.id;
        if (message.channel.type == 'dm') SID = 'dm';
        var cmd = args[0];
        var image = args[1];
        var option1 = args[1];
        var imageLink = args[1];
        var option2 = args[2];
        var levelXP = 200;
        if (cmd != null) cmd = cmd.toLowerCase();
        if (option1 != null) option1 = option1.toLowerCase();
        if (option2 != null) option2 = option2.toLowerCase();
        var enableHighscore = client.config.enableHighscoreInStats;
        //User specific variables
        if (message.mentions.members != null) var name = message.mentions.members.first();
        if (name != null) {
            var user = message.mentions.members.first();
            var name = message.mentions.members.first().user.username;
            var ID = message.mentions.members.first().id;            
            var avatarURL = message.mentions.users.first().avatarURL({ format: 'jpg' });
        }
        if (name == null) {
            var user = message.author;
            var name = message.author.username;
            var ID = message.author.id;
            var avatarURL = message.author.avatarURL({ format: 'jpg' });
        }
        var img = client.db.getEco(SID, user, `stats_img`);
        var xp = client.db.getEco(SID, user, `xp`);
        var balance = client.db.getEco(SID, user, `money`);
        var highscore = client.db.getEco(SID, user, `highscore`);
        var prestigemoney = client.db.getEco(SID, user, `prestigemoney`);
        var prestigexp = client.db.getEco(SID, user, `prestigexp`);
        var insurance = client.db.getEco(SID, user, `insurance`);
        if (insurance != null) {
            insurance = insurance.insurance;
        }
        if (insurance == null) insurance = 0;

        function checkImage(url) {
            var request = new XMLHttpRequest();
            request.open("GET", url, true);
            request.send();
            request.onload = function() {
              status = request.status;
              if (request.status == 200) //if(statusText == OK)
              {
              } else {
                return message.reply(`image not available`)
              }
            }
          }
        

        //Defaults
        if (cmd == 'defaults' && option1 !== 'YES') return message.reply('\nDo you really want to delete your Settings?\nType `stats defaults YES` to confirm');
        if (cmd == 'defaults' && option1 === 'YES') {
            client.db.setEco(SID, message.author, `stats_img`, null);
            client.db.setEco(SID, message.author, `stats_xpbar`, null);
            client.db.setEco(SID, message.author, `stats_text`, null);
            client.db.setEco(SID, message.author, `stats_text_outline`, null);
            return message.reply(`\nYour Settings have been set to the defaults!`);
        }


        //Set Img
        if (cmd == 'image' && imageLink != null) {
            if ((imageLink.startsWith('http') || imageLink.startsWith('https')) && (imageLink.endsWith('.png') || imageLink.endsWith('.jpg') || imageLink.endsWith('.jpeg'))) {
                //checkImage(imageLink);
                client.db.setEco(SID, message.author, `stats_img`, image);
                message.reply(`\nYou set a background Image`);
                return;
            } 
            return message.reply(`\nNot a valid Image or URL\nImages must be .png, .jpg or .jpeg`);
        }
        if (cmd == 'image' && imageLink == null) {
            message.reply('\nSet a custom background image\n`stats image img.url.png`\nImage resolution is 700x250px');
            return;
        }


        //Set XP Bar Color
        if (cmd == 'xpbar' && option1 == null) return message.reply('\nChange the XP bar color\nUse Hex Values `stats xpbar 719b1f`');
        if (cmd == 'xpbar' && option1 != null) {
            var hex = /[0-9A-Fa-f]{6}/g;
            if (hex.test(option1)) {
                hex.lastIndex = 0;
                client.db.setEco(SID, message.author, `stats_xpbar`, option1);
            } else {
                hex.lastIndex = 0;
                return message.reply(`\n${option1} is not a valid hex value\n` + '`stats xpbar 719b1f`');                
            }
            return message.reply(`\nYou changed the progress bar color`);
        }
        var xpBarColor = client.db.getEco(SID, user, `stats_xpbar`);
        if (xpBarColor == null) var xpBarColor = '719b1f';


        //Set Textcolor
		if (cmd == 'text' && option1 == null) return message.reply('\nChange the text color `stats text textColor textOutline`\nUse Hex Values `stats text ffffff 000000`');
        if (cmd == 'text' && option1 != null || option2 != null) {
            var hex = /[0-9A-Fa-f]{6}/g;
            if (hex.test(option1)) {
                hex.lastIndex = 0;
                client.db.setEco(SID, message.author, `stats_text`, option1);
                client.db.setEco(SID, message.author, `stats_text_outline`, option1);
            } else {
                hex.lastIndex = 0;
                return message.reply(`\n${option1} is not a valid hex value\n` + '`stats text ffffff 000000`');
            }
            if (option2 == null) return message.reply(`\nYou changed your text color`);

            if (hex.test(option2)) {
                client.db.setEco(SID, message.author, `stats_text_outline`, option2);
                hex.lastIndex = 0;
            } else {
                hex.lastIndex = 0;
                return message.reply(`\n${option2} is not a valid hex value\n` + '`stats text ffffff 000000`');
            }
            return message.reply(`\nYou changed the text color`);
        }
        var text = client.db.getEco(SID, user, `stats_text`);
        var textOutline = client.db.getEco(SID, user, `stats_text_outline`);
        if (text == null) var text = 'd9d9d9';
        if (textOutline == null) var textOutline = '111111';


        //Calculate Level
        var level = 0;
        var levelStep = 0;
        while (levelStep < xp) {
            var levelStep = levelStep + levelXP * level;
            level++;
        }
        var level = level - 2;
        var levelUp = level + 1;
        if (level < 0) var level = 0;
        if (levelUp < 0) var levelUp = 1;
        if (xp == null) var xp = 0;
        if (levelStep == 0) var levelStep = levelXP;  
        //Calculate XP bar
        var xpBarNeeded = levelXP * level + levelXP;
		var xpStart = levelStep - xpBarNeeded;
		var xpMin = xp - xpStart;
		var xpBar = xpMin / xpBarNeeded * 696;
        //Format Values
        highscore = number.format(highscore);
        insurance = number.format(insurance);
        balance = number.format(balance);
        xp = number.format(xp);
        levelStep = number.format(levelStep);
        //Bot Stats
        if (name != null && ID == client.config.privileges.botID) {
            xp = '∞';
            xpBar = 696;
            level = '∞';
            levelStep = '∞';
        }
        
        
        //Canvas
        //Base Canvas and Background
        if (img == null) img = './assets/lain_default.png'
        const canvas = Canvas.createCanvas(700, 250);
        const ctx = canvas.getContext('2d');
        const background = await Canvas.loadImage(img);
        var fontSizeThin = 500;
        var fontSizeBold = 700;
        ctx.drawImage(background, 0, 0, canvas.width, canvas.height);
        //Username
        ctx.font = `${fontSizeBold} 35px sans-serif`;
        ctx.fillStyle = `#${text}`;
        ctx.fillText(name, 140, 70);
        ctx.strokeStyle = `#${textOutline}`;
        ctx.strokeText(name, 140, 70);
        //Level
        ctx.font = `${fontSizeThin} 25px sans-serif`;
        ctx.strokeStyle = `#${textOutline}`;
        ctx.strokeText(`lvl ${level}`, 140, 105);
        ctx.fillStyle = `#${text}`;
        ctx.fillText(`lvl ${level}`, 140, 105);
        //XP Counter
        ctx.font = `${fontSizeThin} 25px sans-serif`;
        ctx.strokeStyle = `#${textOutline}`;
        ctx.strokeText(`${xp}/${levelStep}XP`, 25, 200);
        ctx.fillStyle = `#${text}`;
        ctx.fillText(`${xp}/${levelStep}XP`, 25, 200);
        //XP Progress bar
        ctx.font = `${fontSizeThin} 25px sans-serif`;
        ctx.fillStyle = '#191919';
        ctx.fillRect(0, 220, 700, 10);
        ctx.fillStyle = `#${xpBarColor}`;
        ctx.fillRect(2, 222, xpBar.toFixed(), 6);
        //Insurance / Highscore
        if (enableHighscore == 0 && insurance != 0) {
            ctx.textAlign = 'right';
            ctx.font = `${fontSizeThin} 25px sans-serif`;
            ctx.strokeStyle = `#${textOutline}`;
            ctx.strokeText(`${insurance}$`, 675, 170);
            ctx.fillStyle = `#${text}`;
            ctx.fillText(`${insurance}$`, 675, 170);
        }
        if (enableHighscore == 1) {
            ctx.textAlign = 'right';
            ctx.font = `${fontSizeThin} 25px sans-serif`;
            ctx.strokeStyle = `#${textOutline}`;
            ctx.strokeText(`${highscore}$`, 675, 170);
            ctx.fillStyle = `#${text}`;
            ctx.fillText(`${highscore}$`, 675, 170);
        }
        //Balance
        ctx.textAlign = 'right';
        ctx.font = `${fontSizeBold} 25px sans-serif`;
        ctx.fillStyle = `#${text}`;
        ctx.fillText(`${balance}$`, 675, 200);
        ctx.strokeStyle = `#${textOutline}`;
        ctx.strokeText(`${balance}$`, 675, 200);
        ctx.textAlign = 'left'; 
        //prestige
        if (prestigemoney == null) prestigemoney = 0;
        if (prestigexp == null) prestigexp = 0;
        const money = await Canvas.loadImage(`./assets/prestige/money/${prestigemoney}.png`);
        const XP = await Canvas.loadImage(`./assets/prestige/xp/${prestigexp}.png`);
        if (prestigemoney > 0 && prestigexp > 0) {
            //draw XP and Money
            ctx.drawImage(money, 23, 118, 52, 52);
            ctx.drawImage(XP, 71, 118, 52, 52);
        }
        if ((prestigemoney == 0 && prestigexp > 0) || (prestigemoney > 0 && prestigexp == 0)) {
            //draw XP or Money
            ctx.drawImage(money, 47, 118, 52, 52);
            ctx.drawImage(XP, 47, 118, 52, 52);
        }
        //Avatar
        ctx.beginPath();
        ctx.arc(73, 73, 48, 0, Math.PI * 2, true);
        ctx.closePath();
        ctx.clip();
        const avatar = await Canvas.loadImage(avatarURL);
        ctx.drawImage(avatar, 25, 25, 96, 96);
        //Send Canvas
        const attachment = new Discord.MessageAttachment(canvas.toBuffer(), `stats_${name}.png`);
        message.channel.send(attachment);
        return;

        //Foreground
        //ctx.save();
        //ctx.translate(730, 60);
        //ctx.rotate(Math.PI / 6);
        //ctx.strokeStyle = `black`;
        //ctx.strokeRect(-40, -40, 80, 350);
        //ctx.fillStyle = `grey`;
        //ctx.fillRect(-40, -40, 80, 350);
        //ctx.restore();
    }
}
