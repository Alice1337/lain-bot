module.exports = {
    name: "slots",
    cooldown: 2,
    run (client, message, args, cooldownTime) {
        if (cooldownTime != 0) return message.reply(`\nplease wait ${cooldownTime} to play again`);
        var number = Intl.NumberFormat();
        if (message.channel.type != 'dm') SID = message.guild.id;
        if (message.channel.type == 'dm') SID = 'dm';
        const Discord = require('discord.js');
        let symbol = [":apple:", ":lemon:", ":watermelon:", ":grapes:", ":cherries:",  ":star:"];// ":tangerine:", ":blueberries:",
        //let symbol = [":apple:", ":star:"];
        let count = args[0];
        let maxSlots = client.config.maxSlots;


        if (count == 0) return message.reply(`\nYou've played 0 slots, nice one`);
        if (count < 0) return message.reply(`\nthat doesn't seem to be right`);
        

        //veto function
        //set name of current game
        client.db.setEco(SID, message.author, `veto_game`, client.commands.get(message.content.slice(prefix.length).trim().split(/ +/g).shift().toLowerCase()).name);
        //set win/loss
        //set +/- depending on the outcome of the game
        function veto_stats(win, status) {
            //veto
            client.db.setEco(SID, message.author, `veto_money`, win);
            client.db.setEco(SID, message.author, `veto_status`, status);
        }
        

        //Single Game
        if ((count == null || count == 0 || count == 1) && (client.db.getEco(SID, message.author, `money`) >= 250)) {
            var win = 0;
            var S = [];
            for (i = 0; i < 9; i++) {
                S[i] = symbol[Math.floor(Math.random() * symbol.length)];
            }

            function embedPost(color, postMessage, win) {
                if (win.startsWith('-')) client.db.addEco(SID, message.author, `xp`, client.db.prestigeXP(SID, message.author, 5));
                if (win.startsWith('+')) client.db.addEco(SID, message.author, `xp`, client.db.prestigeXP(SID, message.author, 10));
                const embedPost = new Discord.MessageEmbed()
                    .setColor(color)
                    .setAuthor(`${message.author.username} played slots`, message.author.avatarURL())
                    .setTitle(`${S[0]} ${S[1]} ${S[2]}\n${S[3]} ${S[4]} ${S[5]}\n${S[6]} ${S[7]} ${S[8]}`)
                    .addField(postMessage, `${win}$`)
                    .setFooter(`Jackpot: ${number.format(client.db.getEco(SID, client.user, `money`))}$`)
                message.channel.send(embedPost);
                return;
            }
            
            //Vollbild
            if (((S[0] == S[1]) && (S[1] == S[2])) && ((S[3] == S[4]) && (S[4] == S[5])) && ((S[6] == S[7]) && (S[7] == S[8]))) {
                win = client.db.getEco(SID, client.user, `money`);
                client.db.subEco(SID, client.user, win);
                win = client.db.prestigeMoney(SID, message.author, win);
                client.db.addEco(SID, message.author, `money`, win);
                //veto
                veto_stats(win, '+');
                embedPost (0xc99400, "Vollbild!", `+${number.format(win)}`);
                return;
            } else
            //Mid line Sterne
            if ((S[3] == ":star:") && (S[4] == ":star:") && (S[5] == ":star:")) {
                win = client.db.getEco(SID, client.user, `money`);
                client.db.subEco(SID, client.user, win);
                win = client.db.prestigeMoney(SID, message.author, win);
                client.db.addEco(SID, message.author, `money`, win);
                //veto
                veto_stats(win, '+');
                embedPost (0xc99400, "Jackpot!", `+${number.format(win)}`);
                return;
            } else
            //Mid Line
            if ((S[3] == S[4]) && (S[4] == S[5])) {
                win = 5000;
                win = client.db.prestigeMoney(SID, message.author, win);
                client.db.addEco(SID, message.author, `money`, win);
                //veto
                veto_stats(win, '+');
                embedPost (0x719b1f, "Big Win!", `+${number.format(win)}`);
                return;
            } else
            //Upper and Lower Line
            if ((((S[0] == S[1]) && (S[1]) == S[2])) && ((S[6] == S[7]) && (S[7] == S[8]))) {
                win = 3000;
                win = client.db.prestigeMoney(SID, message.author, win);
                client.db.addEco(SID, message.author, `money`, win);
                //veto
                veto_stats(win, '+');
                embedPost (0x5886ce, "Two Lines!", `+${number.format(win)}`);
                return;
            }  else 
            //Upper or Lower Line
            if ((((S[0] == S[1]) && (S[1]) == S[2])) || ((S[6] == S[7]) && (S[7] == S[8]))) {
                win = 1500;
                win = client.db.prestigeMoney(SID, message.author, win);
                client.db.addEco(SID, message.author, `money`, win);
                //veto
                veto_stats(win, '+');
                embedPost (0x5886ce, "One Line", `+${number.format(win)}`);
                return;
            } else
            //Nothing
                win = 250;
                client.db.subEco(SID, message.author, `money`, win);
                client.db.addEco(SID, client.user, `money`, win);
                //veto
                veto_stats(win, '-');
                embedPost (0xa82418, "No luck this time", `-250`);
                return;
        } else


        //Multiple Games
        if (count > 1) {
            var win = 0;
            var winSum = 0;
            var color = 0;
            var balance = client.db.getEco(SID, message.author, `money`);
            countMoney = count * 250;
            if ((balance >= countMoney) && (count <= maxSlots)) {
                loop = 0;
                var winArray = [];

                function embedPostMulti(color, postMessage, wins) {
                    const embedPostMulti = new Discord.MessageEmbed()
                        .setColor(color)
                        .setAuthor(`${message.author.username} played slots`, message.author.avatarURL())
                        .addField(`${wins}`, postMessage)
                        .setFooter(`Jackpot: ${number.format(client.db.getEco(SID, client.user, `money`))}$`)
                    message.channel.send(embedPostMulti);
                    return;
                }

                function calculate(win) {
                    //client.db.subEco(SID, client.user, `money`, win)
                    client.db.addEco(SID, message.author, `money`, win);
                    winSum = winSum += parseInt(win);
                    winArray.push(`\n+${number.format(win)}$`);
                    client.db.addEco(SID, message.author, `xp`, client.db.prestigeXP(SID, message.author, 10));
                }

                while (loop < count) {
                    var S = [];
                    for (i = 0; i < 9; i++) {
                        S[i] = symbol[Math.floor(Math.random() * symbol.length)];
                    }

                    //Vollbild
                    if (((S[0] == S[1]) && (S[1] == S[2])) && ((S[3] == S[4]) && (S[4] == S[5])) && ((S[6] == S[7]) && (S[7] == S[8]))) {
                        win = client.db.getEco(SID, client.user, `money`);
                        client.db.subEco(SID, client.user, `money`, win)
                        win = client.db.prestigeMoney(SID, message.author, win);
                        calculate(win);
                        loop++;
                    } else
                    //Mid line Sterne
                    if ((S[3] == ":star:") && (S[4] == ":star:") && (S[5] == ":star:")) {
                        win = client.db.getEco(SID, client.user, `money`);
                        client.db.subEco(SID, client.user, `money`, win)
                        win = client.db.prestigeMoney(SID, message.author, win);
                        calculate(win);
                        loop++;
                    } else
                    //Mid Line
                    if ((S[3] == S[4]) && (S[4] == S[5])) {
                        win = 5000;
                        win = client.db.prestigeMoney(SID, message.author, win);
                        calculate(win);
                        loop++;
                    } else
                    //Upper and Lower Line
                    if (((S[0] == S[1]) && (S[1] == S[2])) && ((S[6] == S[7]) && (S[7] == S[8]))) {
                        win = 3000;
                        win = client.db.prestigeMoney(SID, message.author, win);
                        calculate(win);
                        loop++
                    }  else
                    //Upper or Lower Line
                    if (((S[0] == S[1]) && (S[1] == S[2])) || ((S[6] == S[7]) && (S[7] == S[8]))) {
                        win = 1500;
                        win = client.db.prestigeMoney(SID, message.author, win);
                        calculate(win);
                        loop++;
                    } else
                    //Nothing
                        win = -250;
                        client.db.subEco(SID, message.author, `money`, 250);
                        client.db.addEco(SID, client.user, `money`, 250);
                        winSum = winSum -= 250;
                        winArray.push(`\n-250$`);
                        client.db.addEco(SID, message.author, `xp`, client.db.prestigeXP(SID, message.author, 5));
                        loop++;
                    }

                    if (winSum > 0) {
                        color = 0x718f32;
                        jackpot = client.db.getEco(SID, client.user, `money`);
                        win = client.db.getEco(SID, client.user, `money`);
                        win = winSum;
                        winSum = number.format(winSum);
                        postMessage = `You Won ${winSum}$`;
                        //veto
                        veto_stats(win, '+');
                        embedPostMulti (color, `${postMessage}`, `${winArray}`, `${jackpot}`);
                    } else
                    if (winSum < 0) {
                        color = 0xa82418;
                        jackpot = client.db.getEco(SID, client.user, `money`);
                        win = client.db.getEco(SID, client.user, `money`);
                        win = winSum - winSum - winSum;
                        winSum = number.format(winSum);
                        postMessage = `You lost ${winSum}$`;
                        //veto
                        veto_stats(win, '-');
                        embedPostMulti (color, `${postMessage}`, `${winArray}`, `${jackpot}`);
                    } else
                    if (winSum == 0) {
                        postMessage = `You Neither won nor lost`;
                        color = 0x5886ce;
                        jackpot = client.db.getEco(SID, client.user, `money`);
                        win = client.db.getEco(SID, client.user, `money`);
                        //veto
                        veto_stats(win, '+');
                        embedPostMulti (color, `${postMessage}`, `${winArray}`, `${jackpot}`);
                    }
                return;

            }
            if (count > maxSlots) return message.reply(`\nYou can't play more than ${maxSlots} Slots`);
            return message.reply(`\nYou need at least ${number.format(countMoney)}$ to play ${count} Slots`);

        }
        return message.reply(`\nYou don't have enough money\nYour balance is ${number.format(balance)}`);
    }
}
