exports.run = (client, message, args) => {
    var number = Intl.NumberFormat();
    if (message.channel.type != 'dm') SID = message.guild.id;
    if (message.channel.type == 'dm') SID = 'dm';
    var option = args[0];
    if (option != null) option = option.toLowerCase();
    var balance = client.db.getEco(SID, message.author, `money`);
    var money = args[0];
    if (money != null) money = money.toLowerCase();
    var xp = args[1];
    if (xp != null) xp = xp.toLowerCase();
    //var user = args[1];
    if (message.mentions.members != null) var user = message.mentions.members.first();
    if (message.mentions.members != null) var name = message.mentions.members.first();


    if (money == 'xp') {
        var balance = client.db.getEco(SID, message.author, `xp`);

        if (message.mentions.members == null || isNaN(xp)) return message.reply('\nDo `' + client.config.botSettings.prefix + 'send xp 500 @user` to send 500 XP to another user');
        if (xp <= 0) return message.reply(`\nthat doesn't seem to be right`);
        if (balance < xp) return message.reply(`\nYou don't have that much XP to give`);

        if (name == null) return;
        
        if (option == 'xp') {
            var user = message.mentions.members.first().user;
            var name = message.mentions.members.first().user.username;
            var nameID = message.mentions.members.first().id;
            var xpFormat = number.format(xp);
            client.db.subEco(SID, message.author, `xp`, xp);
            client.db.addEco(SID, user, `xp`, xp);
            message.channel.send(`Hey ${user}, ${message.author.username} sent you ${xpFormat} XP`);     
            return;
    
        }        
    }
    

    if (message.mentions.members == null || isNaN(money)) return message.reply('\nDo `' + client.config.botSettings.prefix + 'send 500 @user` to send 500$ to another user\nDo `' + client.config.botSettings.prefix + 'send xp 500 @user` to send 500 XP to another user');
    if (money <= 0) return message.reply(`\nthat doesn't seem to be right`);
    if (balance < money) return message.reply(`\nYou don't have that much money to send`);
    

    if (name == null) return;

    if (name != null) {
        var user = message.mentions.members.first().user;
        var name = message.mentions.members.first().user.username;
        var nameID = message.mentions.members.first().id;
        var moneyFormat = number.format(money);
        client.db.subEco(SID, message.author, `money`, money);
        client.db.addEco(SID, user, `money`, money);
        message.channel.send(`Hey ${user}, ${message.author.username} sent you ${moneyFormat}$`);     
        return;
    }
}