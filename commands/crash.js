module.exports = {
    name: "crash",
    cooldown: 0,
	async run (client, message, args, cooldownTime) {
        //return message.channel.send(`crash is currently disabled`);
        //Cooldown
        if (cooldownTime != 0) return message.reply(`\nplease wait ${cooldownTime} seconds to exchange again`);

        //Default constants
        const Discord = require('discord.js');
        const Canvas = require('canvas');
        const number = Intl.NumberFormat();
        if (message.channel.type != 'dm') SID = message.guild.id;
        if (message.channel.type == 'dm') SID = 'dm';

        //Variables
        var money = args[0];
        var lowerLimit = client.config.lowerLimit;
        var balance = client.db.getEco(SID, message.author, `money`);
        var gameName = client.commands.get(message.content.slice(prefix.length).trim().split(/ +/g).shift().toLowerCase()).name;
        var counterMin = client.config.crash.counterMin;
        var counterMax = client.config.crash.counterMax;
        var counterTime = client.config.crash.timer;
        var divider = client.config.crash.divider;

        //Check money and balance
        if (isNaN(money)) return message.reply("\nYou can't play for free\nDo `" + gameName + " 500` to start playing");
        if (money < 0) return message.reply(`\nthat doesn't seem to be right`);
        if (money < lowerLimit) return message.reply(`\nYou need at least ${lowerLimit}$ to play`);
        if (balance < money) return message.reply(`\nYou don't have enough Money to play\ncurrent balance: ${number.format(balance)}$`);


        //veto function
        client.db.setEco(SID, message.author, `veto_game`, gameName);
        //set the money won/lost with win
        //set status + if the user has won and - if the user has lost
        function veto_stats(win, status) {
            client.db.setEco(SID, message.author, `veto_money`, win);
            client.db.setEco(SID, message.author, `veto_status`, status);
        }

        async function sleep(ms) {
            return new Promise(resolve => setTimeout(resolve, ms));
        }


        //generate counter value
        var counter = Math.random()*(counterMax-counterMin) + counterMin;
        counter = counter.toFixed(0);
        var score = 0;

        
        //Embed Ongoing
        var embedPost = new Discord.MessageEmbed()
            .setColor(0x5886ce)
            .setAuthor(`${message.author.username} is playing crash`, message.author.avatarURL())
            .setDescription(`Stop before the crash to multiply your money`)
        const reactionMessage = await message.channel.send(embedPost);
        await reactionMessage.react("🟢");


        //Count and wait for player input
        for (i = 10; i < counter; i++) {
            await sleep(counterTime);

            const filter = (reaction, user) => {
                return ['🟢', '🔴'].includes(reaction.emoji.name) && user.id === message.author.id;
            };
    
            await reactionMessage.awaitReactions(filter, { max: 1, time: counter, errors: ['time'] })
                .then(collected => {
                    const reaction = collected.first();
                
                    if (reaction.emoji.name === '🟢') {
                        score = i;
                        i = counter;
                    }
                })
                .catch(collected => {
                });
                if (score > 0) break;
        }
        reactionMessage.delete();

        //Crash
        if (score == 0) {
            win = money;
            //Embed Lose
            var embedPost = new Discord.MessageEmbed()
                .setColor(0xa82418)
                .setAuthor(`${message.author.username} played crash`, message.author.avatarURL())
                .addField(`Crashed!`, `You lost ${number.format(win)}$`)
            message.channel.send(embedPost);
            client.db.subEco(SID, message.author, `money`, win);
            client.db.addEco(SID, message.author, `xp`, client.db.prestigeXP(SID, message.author, 5));
            veto_stats(win, '-');
            return;
        }

        //< 1.0 multiplier
        if (score < divider) {
            var win = score / divider * money;
            win = money - win;
            win = win.toFixed(0);
            //Embed 1.0 multiplier
            var embedPost = new Discord.MessageEmbed()
                .setColor(0x141414)
                .setAuthor(`${message.author.username} played crash`, message.author.avatarURL())
                .setDescription(`Multiplier too low`)
                .addField(`x${score/divider} multiplier`, `You lost ${number.format(win)}$`)
            message.channel.send(embedPost);
            client.db.subEco(SID, message.author, `money`, win);
            client.db.addEco(SID, message.author, `xp`, client.db.prestigeXP(SID, message.author, 5));
            veto_stats(win, '-');
            return;
        }

        //< 1.0 multiplier
        if (score == divider) {
            var win = score / divider * money;
            win = money - win;
            win = win.toFixed(0);
            //Embed < 1.0 multiplier
            var embedPost = new Discord.MessageEmbed()
                .setColor(0x5886ce)
                .setAuthor(`${message.author.username} played crash`, message.author.avatarURL())
                .setDescription(`x1 multiplier`)
                .addField(`x${score/divider} multiplier`, `You lost nothing`)
            message.channel.send(embedPost);
            client.db.addEco(SID, message.author, `xp`, client.db.prestigeXP(SID, message.author, 5));
            veto_stats(win, '-');
            return;
        }

        //Win
        if (score > divider) {
            var win = score / divider * money;
            var win = win - money;
            win = client.db.prestigeMoney(SID, message.author, win);
            //Embed Win
            var embedPost = new Discord.MessageEmbed()
                .setColor(0x719b1f)
                .setAuthor(`${message.author.username} played crash`, message.author.avatarURL())
                .addField(`x${score/divider} multiplier`, `You Won ${number.format(win)}$`)
            message.channel.send(embedPost);
            client.db.addEco(SID, message.author, `money`, win);
            client.db.addEco(SID, message.author, `xp`, client.db.prestigeXP(SID, message.author, 10));
            veto_stats(win, '+');
            return;
        }
    }        
}


/*
Hex colors
    Red      
    Black   0x141414
    Green   0x719b1f
    Blue    0x5886ce
    Yellow  0xc99400
*/