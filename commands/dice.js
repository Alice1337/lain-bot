module.exports = {
    name: "dice",
    cooldown: 3,
    run (client, message, args, cooldownTime) {
        if (cooldownTime == 0) {
            const Discord = require('discord.js');
            var number = Intl.NumberFormat();
            if (message.channel.type != 'dm') SID = message.guild.id;
            if (message.channel.type == 'dm') SID = 'dm';
            var dice = ["dice1.png", "dice2.png", "dice3.png", "dice4.png", "dice5.png", "dice6.png"];
            var balance = client.db.getEco(SID, message.author, `money`);
            var user = (message.author.username);
            var roll = args[0];
            var money = args[1];
            var lowerLimit = 250;


            if (isNaN(money)) return message.reply(`\n${money} is not a valid number`);
            if (money < 0) return message.reply(`\nthat doesn't seem to be right`);
            if (money < lowerLimit) return message.reply(`\nYou need at least ${lowerLimit}$ to play`);

        
            //veto function
            //set name of current game
            client.db.setEco(SID, message.author, `veto_game`, client.commands.get(message.content.slice(prefix.length).trim().split(/ +/g).shift().toLowerCase()).name);
            //set win/loss
            //set +/- depending on the outcome of the game
            function veto_stats(win, status) {
                //veto
                client.db.setEco(SID, message.author, `veto_money`, win);
                client.db.setEco(SID, message.author, `veto_status`, status);
            }
        
            if ((balance >= money) && (roll != null) && ((roll <= 6) && (roll >= 1))) {
                var dice = dice[Math.floor(Math.random() * dice.length)];
                if (dice == `dice${roll}.png`) {
                    win = money * 6;
                    win = client.db.prestigeMoney(SID, message.author, win);
                    client.db.addEco(SID, message.author, `money`, win);
                    //veto
                    veto_stats(win, '+');
                    if (roll == 1) {
                        var eyes = `${roll} eye`;
                    } else
                    var eyes = `${roll} eyes`;
                    client.db.addEco(SID, message.author, `xp`, client.db.prestigeXP(SID, message.author, 10));
                    var embedPost = new Discord.MessageEmbed()
                        .setColor(0x719b1f)
                        .setAuthor(`${user} played dice`, message.author.avatarURL())
                        .setDescription(`You won ${number.format(win)}$`)
                        .attachFiles(`./assets/dice/${dice}`)
                        .setImage(`attachment://${dice}`);
                    message.channel.send(embedPost).catch(console.error);
                    return;
                } else
                    client.db.subEco(SID, message.author, `money`, money);
                    //veto
                    veto_stats(money, '-');
                    if (roll == 1) {
                        var eyes = `${roll} eye`;
                    } else
                    var eyes = `${roll} eyes`;
                    client.db.addEco(SID, message.author, `xp`, client.db.prestigeXP(SID, message.author, 5));
                    var embedPost = new Discord.MessageEmbed()
                        .setColor(0xa82418)
                        .setAuthor(`${user} played dice`, message.author.avatarURL())
                        .setDescription(`You lost ${number.format(money)}$`)
                        .attachFiles(`./assets/dice/${dice}`)
                        .setImage(`attachment://${dice}`);
                    message.channel.send(embedPost).catch(console.error);
                    return;


            } else
            if ((money == null) || (roll == null)) return message.reply(`\nplease specify your roll and how much you wanna bet\n'${client.config.botSettings.prefix}dice 5 1000'\n${client.config.botSettings.prefix}help dice for more Info`);
            if ((roll < 1) || (roll > 6)) return message.reply(`\nYour roll must be between 1 and 6`);
            if (balance < money) return message.reply(`\nYou don't have that much money\nYour balance is ${balance}`);


        } return message.reply(`\nplease wait ${cooldownTime} to play again`);
    }
}