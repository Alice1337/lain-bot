module.exports = {
    name: "spin",
    cooldown: 86400,
    //cooldown: 10,
    run (client, message, args, cooldownTime) {
        const Discord = require('discord.js');
        var number = Intl.NumberFormat();
        if (message.channel.type != 'dm') SID = message.guild.id;
        if (message.channel.type == 'dm') SID = 'dm';
        var spin = ["respin1", "500000", "200", "75000", "25000", "10000", "250000", "500", "respin2", "1000000", "100", "50000", "5000", "100000", "1000"];
        var spin = spin[Math.floor(Math.random() * spin.length)];
        var winFormat = number.format(client.db.prestigeMoney(SID, message.author, spin));

        var respin = client.db.getEco(SID, message.author, `luckywheel_respin`);
        var freespin = client.db.getEco(SID, message.author, `luckywheel_freespin`);
        if (respin == null) client.db.setEco(SID, message.author, `luckywheel_respin`, 1);
        if (freespin == null) client.db.setEco(SID, message.author, `luckywheel_freespin`, 0);
        //if (freespin == '1') client.db.setEco(SID, message.author, `cooldown_spin`, ``);

        //veto function
        //set name of current game
        //client.db.setEco(SID, message.author, `veto_game`, client.commands.get(message.content.slice(prefix.length).trim().split(/ +/g).shift().toLowerCase()).name);
        //set win/loss
        //set +/- depending on the outcome of the game
        function veto_stats(win, status) {
            //veto
            client.db.setEco(SID, message.author, `veto_money`, win);
            client.db.setEco(SID, message.author, `veto_status`, status);
        }
        
        if (cooldownTime == 0 || respin == 1 || freespin == 1) {
            if (spin === 'respin1' || spin === 'respin2') {
                client.db.setEco(SID, message.author, `luckywheel_respin`, 1);
                client.db.addEco(SID, message.author, `xp`, client.db.prestigeXP(SID, message.author, 5));
                client.db.setEco(SID, message.author, `luckywheel_multi`, 1);
    
                var embedPost = new Discord.MessageEmbed()
                    .setColor(0x5886ce)
                    .setAuthor(`${message.author.username} turned the lucky wheel`, message.author.avatarURL())
                    .setDescription(`Next one gives double`)
                    .attachFiles(`./assets/spin/wheel_${spin}.png`)
                    .setImage(`attachment://wheel_${spin}.png`)
                message.channel.send(embedPost)
                return;
            }


            var multi = client.db.getEco(SID, message.author, `luckywheel_multi`);
            if (multi == 1) {
                var spinWin = spin * 2;
                var winFormat = spin * 2;
                var winFormat =  client.db.prestigeMoney(SID, message.author, winFormat);
                var winFormat = number.format(winFormat);
                client.db.setEco(SID, message.author, `luckywheel_multi`, 0);
                client.db.addEco(SID, message.author, `money`, client.db.prestigeMoney(SID, message.author, spinWin));
                client.db.setEco(SID, message.author, `luckywheel_respin`, 0);
                client.db.setEco(SID, message.author, `luckywheel_freespin`, 0);
                client.db.addEco(SID, message.author, `xp`, client.db.prestigeXP(SID, message.author, 20));

                var embedPost = new Discord.MessageEmbed()
                    .setColor(0xc99400)
                    .setAuthor(`${message.author.username} turned the lucky wheel`, message.author.avatarURL())
                    .addField(`x2 Multiplicator`, `You won ${winFormat}$`)
                    .attachFiles(`./assets/spin/wheel_${spin}.png`)
                    .setImage(`attachment://wheel_${spin}.png`)
                    message.channel.send(embedPost)
                return;
            }

            client.db.addEco(SID, message.author, `money`, client.db.prestigeMoney(SID, message.author, spin));
            client.db.setEco(SID, message.author, `luckywheel_respin`, 0);
            client.db.setEco(SID, message.author, `luckywheel_freespin`, 0);
            client.db.addEco(SID, message.author, `xp`, client.db.prestigeXP(SID, message.author, 20));
 
            var embedPost = new Discord.MessageEmbed()
                .setColor(0xc99400)
                .setAuthor(`${message.author.username} turned the lucky wheel`, message.author.avatarURL())
                .setDescription(`You won ${winFormat}$`)
                .attachFiles(`./assets/spin/wheel_${spin}.png`)
                .setImage(`attachment://wheel_${spin}.png`)
            message.channel.send(embedPost)
            return;


        } else
        if (cooldownTime <= 60) return message.reply(`\nYou already turned the lucky wheel\nplease wait ${cooldownTime} seconds to spin it again`);
        if (cooldownTime <= 3600) {
            cooldownTime = cooldownTime / 60;
            message.reply(`\nYou already turned the lucky wheel\nplease wait ${cooldownTime.toFixed(0)} minutes to spin it again`);
            return;
        } else
        if (cooldownTime > 3600) {
            cooldownTime = cooldownTime / 60 / 60;
            message.reply(`\nYou already turned the lucky wheel\nplease wait ${cooldownTime.toFixed(1)} hours to spin it again`);
            return;
        }
    }
}
