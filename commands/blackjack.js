module.exports = {
    name: "blackjack",
    cooldown: 3,
	async run (client, message, args, cooldownTime) {
        //Cooldown
        if (cooldownTime != 0) return message.reply(`\nplease wait ${cooldownTime} seconds to play again`);

        //Default constants
        const Discord = require('discord.js');
        const Canvas = require('canvas');
        const number = Intl.NumberFormat();
        if (message.channel.type != 'dm') SID = message.guild.id;
        if (message.channel.type == 'dm') SID = 'dm';

        //Variables
        var money = args[0];
        var lowerLimit = client.config.lowerLimit;
        var balance = client.db.getEco(SID, message.author, `money`);
        var gameName = client.commands.get(message.content.slice(prefix.length).trim().split(/ +/g).shift().toLowerCase()).name;
        var editEmbed = client.config.editBlackjackEmbeds;

        //if (args[1] != null) editEmbed = 1;

        //reset
        if (money != null) var option = money.toLowerCase();
        if (option == 'reset' || client.db.getEco(SID, message.author, `blackjackMoney`) == null) {
            var blackjackMoney = client.db.getEco(SID, message.author, `blackjackMoney`);
            if (blackjackMoney != null && blackjackMoney > 0) {
                client.db.subEco(SID, message.author, `money`, blackjackMoney);
            }
            client.db.setEco(SID, message.author, `blackjackMoney`, `0`);
            if (option == 'reset') {
                message.reply(`\nYour game has been reset\nYou lost ${number.format(blackjackMoney)}$ from the ongoing game`);
                return;
            }
        }

        //Check money and balance
        if (isNaN(money)) return message.reply("\nYou can't play for free\nDo `" + gameName + " 500` to start playing");
        if (money < 0) return message.reply(`\nthat doesn't seem to be right`);
        if (money < lowerLimit) return message.reply(`\nYou need at least ${lowerLimit}$ to play`);
        if (balance < money) return message.reply(`\nYou don't have enough Money to play\ncurrent balance: ${number.format(balance)}$`);


        //veto function
        client.db.setEco(SID, message.author, `veto_game`, gameName);
        function veto_stats(win, status) {
            client.db.setEco(SID, message.author, `veto_money`, win);
            client.db.setEco(SID, message.author, `veto_status`, status);
        }

        //Get Winner
        function getWinner(playerSum, dealerSum, playerBlackjack, dealerBlackjack, money, blackjackHold) {
            var win = 0;
            var postMessage = 'error';
            var postMessageAlt = 'error';
            var color = 0x5886ce;
            var endGame = 0;
            var won = 0;
            //Blackjack tie
            if (playerBlackjack == 1 && dealerBlackjack == 1) {
                win = 0;
                postMessage = `Tie`;
                postMessageAlt = 'Blackjack Tie';
                color = 0x141414;
                endGame = 1;
                won = 'tie';
            }
            //Player Blackjack
            if (playerBlackjack == 1 && dealerBlackjack == 0) {
                money = client.db.prestigeMoney(SID, message.author, money);
                win = money * 3;
                postMessage = `You Won ${number.format(win)}$`;
                postMessageAlt = `Player Blackjack`;
                color = 0x719b1f;
                endGame = 1;
                won = 1;
            }
            //Dealer Blackjack
            if (playerBlackjack == 0 && dealerBlackjack == 1) {
                win = money;
                postMessage = `You lost ${number.format(win)}$`;
                postMessageAlt = `Dealer Blackjack`;
                color = 0xa82418;
                endGame = 1;
                won = 0;
            }
            //Tie Hold
            if (playerSum == dealerSum && blackjackHold == 1 && (playerBlackjack == 0 && dealerBlackjack == 0))  {
                win = 0;
                postMessage = `Tie`;
                postMessageAlt = 'Tie';
                color = 0x141414;
                endGame = 1;
                won = 'tie';
            }
            //Tie 21
            if  (playerSum == 21 && dealerSum == 21 && (playerBlackjack == 0 && dealerBlackjack == 0))  {
                win = 0;
                postMessage = `Tie`;
                postMessageAlt = 'Tie';
                color = 0x141414;
                endGame = 1;
                won = 'tie';
            }
            //Hold Win
            if (((playerSum <= 21 && dealerSum < playerSum) || (playerSum <= 21 && dealerSum > 21)) && blackjackHold == 1 && (playerBlackjack == 0 && dealerBlackjack == 0)) {
                money = client.db.prestigeMoney(SID, message.author, money);
                win = money;
                postMessage = `You Won ${number.format(win)}$`;
                postMessageAlt = `Dealer lost`;
                color = 0x719b1f;
                endGame = 1;
                won = 1;
            }
            //Hold lost
            if (playerSum > 21 && dealerSum <= 21 && blackjackHold == 1 && (playerBlackjack == 0 && dealerBlackjack == 0)) {
                win = money;
                postMessage = `You lost ${number.format(win)}$`;
                postMessageAlt = `Dealer Won`;
                color = 0xa82418;
                endGame = 1;
                won = 0;
            }
            //Player Win
            if (playerSum <= 21 && playerSum > dealerSum && blackjackHold == 1 && (playerBlackjack == 0 && dealerBlackjack == 0)) {
                money = client.db.prestigeMoney(SID, message.author, money);
                win = money;
                postMessage = `You Won ${number.format(win)}$`;
                postMessageAlt = `Player Won`;
                color = 0x719b1f;
                endGame = 1;
                won = 1;
            }
            //Player lost
            if (dealerSum <= 21 && dealerSum > playerSum && blackjackHold == 1 && (playerBlackjack == 0 && dealerBlackjack == 0)) {
                win = money;
                postMessage = `You lost ${number.format(win)}$`;
                postMessageAlt = `Dealer Won`;
                color = 0xa82418;
                endGame = 1;
                won = 0;
            }
            //Both lost
            if (playerSum > 21 && dealerSum > 21 && (playerBlackjack == 0 && dealerBlackjack == 0)) {
                win = money;
                postMessage = `You lost ${number.format(win)}$`;
                postMessageAlt = `Player and Dealer lost`;
                color = 0xa82418;
                endGame = 1;
                won = 0;
            }
            //Player lost
            if (dealerSum <= 21 && dealerSum > playerSum && blackjackHold == 1 && (playerBlackjack == 0 && dealerBlackjack == 0)) {
                win = money;
                postMessage = `You lost ${number.format(win)}$`;
                postMessageAlt = `Dealer Won`;
                color = 0xa82418;
                endGame = 1;
                won = 0;
            }
            return [postMessage, endGame, won, win, postMessageAlt, color];
        }


        //Variables
        client.db.setEco(SID, message.author, `blackjackMoney`, money);
        var blackjackMoney = client.db.getEco(SID, message.author, `blackjackMoney`);
        var cardAmount = 9;
        var cards = ["Ad", "2d", "3d", "4d", "5d", "6d", "7d", "8d", "9d", "10d", "Jd", "Qd", "Kd", "Ah", "2h", "3h", "4h", "5h", "6h", "7h", "8h", "9h", "10h", "Jh", "Qh", "Kh", "Ac", "2c", "3c", "4c", "5c", "6c", "7c", "8c", "9c", "10c", "Jc", "Qc", "Kc", "As", "2s", "3s", "4s", "5s", "6s", "7s", "8s", "9s", "10s", "Js", "Qs", "Ks"];


        //Generate Cards
        if (client.db.getEco(SID, message.author, `blackjackMoney`) != 0) {
            //Card Arrays
            //Player Card Variables
            var playerDeck = [];
            var playerDeckValues = [];
            //Dealer Card Variables
            var dealerDeck = [];
            var dealerDeckValues = [];

            //Generate Player Cards
            for (i = 0; i < cardAmount; i++) {
                playerDeck[i] = cards[Math.floor(Math.random() * cards.length)];
                playerDeckValues.push(playerDeck[i]);
            }
            //Generate Dealer Cards
            for (i = 0; i < cardAmount; i++) {
                dealerDeck[i] = cards[Math.floor(Math.random() * cards.length)];
                dealerDeckValues.push(dealerDeck[i]);
            }

            //Calculate Card Values
            for (i = 0; i < cardAmount; i++) {
                //Variables
                //Player Card Variables
                var playerValues = [];
                var playerBlackjack = 0;
                var playerSplit = 0;
                //Dealer Card Variables
                var dealerValues = [];
                var dealerBlackjack = 0;
                var dealerSplit = 0;
    
                //Calculate Player Card Values
                for (i = 0; i < cardAmount; i++) {
                    playerDeckValues[i] = playerDeck[i].slice(0, 1);
                    if (playerDeck[i].startsWith('A') && i <= 1) playerDeckValues[i] = 11;
                    if (playerDeck[i].startsWith('J') && i <= 1) playerDeckValues[i] = 10;
                    if (playerDeck[i].startsWith('J') || playerDeck[i].startsWith('Q') || playerDeck[i].startsWith('K') || playerDeck[i].startsWith('10')) playerDeckValues[i] = 10;
                    if (playerDeck[i].startsWith('A') && i > 1) playerDeckValues[i] = 1;
                    playerValues.push(playerDeckValues[i]);
                }
                if (playerDeck[0].startsWith('A') && playerDeck[1].startsWith('A')) {
                    playerValues[1] = 1;
                    playerSplit = 1;
                }
                if (playerDeck[0].startsWith('A') && playerDeck[1].startsWith('J')) playerBlackjack = 1;
                if (playerDeck[0].startsWith('J') && playerDeck[1].startsWith('A')) playerBlackjack = 1;

                //Calculate Dealer Card Values
                for (i = 0; i < cardAmount; i++) {
                    dealerDeckValues[i] = dealerDeck[i].slice(0, 1);
                    if (dealerDeck[i].startsWith('A') && i <= 1) dealerDeckValues[i] = 11;
                    if (dealerDeck[i].startsWith('J') && i <= 1) dealerDeckValues[i] = 10;
                    if (dealerDeck[i].startsWith('J') || dealerDeck[i].startsWith('Q') || dealerDeck[i].startsWith('K') || dealerDeck[i].startsWith('10')) dealerDeckValues[i] = 10;
                    if (dealerDeck[i].startsWith('A') && i > 1) dealerDeckValues[i] = 1;
                    dealerValues.push(dealerDeckValues[i]);
                }
                if (dealerDeck[0].startsWith('A') && dealerDeck[1].startsWith('A')) {
                    dealerValues[1] = 1;
                    dealerSplit = 1;
                }
                if (dealerDeck[0].startsWith('A') && dealerDeck[1].startsWith('J')) dealerBlackjack = 1;
                if (dealerDeck[0].startsWith('J') && dealerDeck[1].startsWith('A')) dealerBlackjack = 1;
            } 
        }

        //Variables
        var blackjackHold = 0;
        var playerStep = 2;
        var dealerStep = 2;
        var playerSum = 0;
        var dealerSum = 0;
        while (blackjackHold == 0) {
            //Calculate Sum
            if (playerStep == 2 && playerSum == 0 && dealerSum == 0) {
                playerSum = playerSum + parseInt(playerValues[0]) + parseInt(playerValues[1]);
                dealerSum = dealerSum + parseInt(dealerValues[0]) + parseInt(dealerValues[1]);

                result = getWinner(playerSum, dealerSum, playerBlackjack, dealerBlackjack, money, blackjackHold);
                if (result[1] == 1) blackjackHold = 1;
                if (result[1] == 1) break;
            }

            //Canvas
            //Base Canvas and Background
            var img = './assets/background.png';
            const canvas = Canvas.createCanvas(512, 512);
            const ctx = canvas.getContext('2d');
            const background = await Canvas.loadImage(img);
            ctx.drawImage(background, 0, 0, canvas.width, canvas.height);
            var fontSizeThin = 600;
            var fontSizeBold = 600;
            //Game Text
            ctx.textAlign = 'center';
            ctx.font = `${fontSizeBold} 59px sans-serif`;
            ctx.strokeStyle = '#40195C';
            ctx.strokeText(`lainchan`, 256, 430);
            ctx.fillStyle = '#40195C';
            ctx.fillText(`lainchan`, 256, 430);
            ctx.font = `${fontSizeThin} 23px sans-serif`;
            ctx.strokeStyle = '#393939';
            ctx.strokeText(`B  L  A  C  K  J  A  C  K`, 256, 465);
            ctx.fillStyle = '#787878';
            ctx.fillText(`B  L  A  C  K  J  A  C  K`, 256, 465);
            ctx.textAlign = 'right';
            //Draw Player Cards
            const playerCard1 = await Canvas.loadImage(`./assets/cards/${playerDeck[0]}.png`);
            ctx.drawImage(playerCard1, 75, 50, 93, 135);
            const playerCard2 = await Canvas.loadImage(`./assets/cards/${playerDeck[1]}.png`);
            ctx.drawImage(playerCard2, 115, 50, 93, 135);

            var renderPlayerCards = [];
            for (i = 2; i < playerStep; i++) {
                var position = 40 * i + 75;
                renderPlayerCards[i] = `./assets/cards/blank.png`;
                if (playerStep >= i) renderPlayerCards[i] = `./assets/cards/${playerDeck[i]}.png`;
                renderPlayerCards[i] = await Canvas.loadImage(renderPlayerCards[i]);
                ctx.drawImage(renderPlayerCards[i], position, 50, 93, 135);
                if (i >= cardAmount) break;
            }
            //Draw Dealer Cards
            var getDealerCards1 = `./assets/cards/backside.png`;
            if (dealerSum >= 21) getDealerCards1 = `./assets/cards/${dealerDeck[0]}.png`;
            const dealerCards1 = await Canvas.loadImage(getDealerCards1);
            ctx.drawImage(dealerCards1, 75, 220, 93, 135);
            const dealerCards2 = await Canvas.loadImage(`./assets/cards/${dealerDeck[1]}.png`);
            ctx.drawImage(dealerCards2, 115, 220, 93, 135);

            var renderDealerCards = [];
            for (i = 2; i < dealerStep; i++) {
                var position = 40 * i + 75;
                renderDealerCards[i] = `./assets/cards/blank.png`;
                if (dealerStep > i) renderDealerCards[i] = `./assets/cards/${dealerDeck[i]}.png`;
                renderDealerCards[i] = await Canvas.loadImage(renderDealerCards[i]);
                ctx.drawImage(renderDealerCards[i], position, 220, 93, 135);
                if (i >= cardAmount) break;
            }
            const attachment = new Discord.MessageAttachment(canvas.toBuffer(), `blackjack.png`);

            //Embed Ongoing
            var embedPost = new Discord.MessageEmbed()
                .setColor(0x5886ce)
                .setAuthor(`${message.author.username} is playing blackjack`, message.author.avatarURL())
                .addField(`Your Cards ${playerSum}`, `Dealers Cards ${dealerSum - dealerValues[0]}?`)
                .attachFiles(attachment)
                .setImage('attachment://blackjack.png')
            const reactionMessage = await message.channel.send(embedPost);
            await reactionMessage.react('👍'); //Draw
            await reactionMessage.react('👎'); //Hold

            const filter = (reaction, user) => {
                return ['👍', '👎'].includes(reaction.emoji.name) && user.id === message.author.id;
            };

            await reactionMessage.awaitReactions(filter, { max: 1, time: 25000, errors: ['time'] })
                .then(collected => {
                    const reaction = collected.first();
                    
                    if (reaction.emoji.name === '👍') {
                        //Calculate Sum
                        playerStep++;
                        playerSum += parseInt(playerValues[playerStep - 1]);
                        if (dealerSum < 17) {
                            dealerStep++;
                            dealerSum += parseInt(dealerValues[dealerStep - 1]);
                        }
                        if (playerSum >= 21 || dealerSum >= 21) blackjackHold = 1;
                        //result = getWinner(playerSum, dealerSum, playerBlackjack, dealerBlackjack, money, blackjackHold);
                        if (editEmbed == 1) reactionMessage.delete();
                    }

                    if (reaction.emoji.name === '👎') {
                        blackjackHold = 1;
                        //Draw remaining dealer cards
                        for (i = 0; dealerSum < 17; dealerStep++) {
                            dealerSum += parseInt(dealerValues[dealerStep]);
                        }
                        //result = getWinner(playerSum, dealerSum, playerBlackjack, dealerBlackjack, money, blackjackHold);
                        if (editEmbed == 1) reactionMessage.delete();
                    }  
                })
                .catch(collected => {
                    blackjackHold = 1;
                    //Draw remaining dealer cards
                    for (dealerStep = playerStep; dealerSum < 17; dealerStep++) {
                        dealerSum += parseInt(dealerValues[dealerStep]);
                    }
                    //result = getWinner(playerSum, dealerSum, playerBlackjack, dealerBlackjack, money, blackjackHold);
                        if (editEmbed == 1) reactionMessage.delete();
            });
            result = getWinner(playerSum, dealerSum, playerBlackjack, dealerBlackjack, money, blackjackHold);
            if (result[1] == 1) blackjackHold = 1;
            if (blackjackHold == 1) break;
            //if (editEmbed = 1)reactionMessage.delete();
        }


        if (blackjackHold == 1) {
            //Canvas
            //Base Canvas and Background
            var img = './assets/background.png';
            const canvas = Canvas.createCanvas(512, 512);
            const ctx = canvas.getContext('2d');
            const background = await Canvas.loadImage(img);
            ctx.drawImage(background, 0, 0, canvas.width, canvas.height);
            var fontSizeThin = 600;
            var fontSizeBold = 600;
            //Game Text
            ctx.textAlign = 'center';
            ctx.font = `${fontSizeBold} 59px sans-serif`;
            ctx.strokeStyle = '#40195C';
            ctx.strokeText(`lainchan`, 256, 430);
            ctx.fillStyle = '#40195C';
            ctx.fillText(`lainchan`, 256, 430);
            ctx.font = `${fontSizeThin} 23px sans-serif`;
            ctx.strokeStyle = '#393939';
            ctx.strokeText(`B  L  A  C  K  J  A  C  K`, 256, 465);
            ctx.fillStyle = '#787878';
            ctx.fillText(`B  L  A  C  K  J  A  C  K`, 256, 465);
            ctx.textAlign = 'right';
            //Draw Player Cards
            const playerCard1 = await Canvas.loadImage(`./assets/cards/${playerDeck[0]}.png`);
            ctx.drawImage(playerCard1, 75, 50, 93, 135);
            const playerCard2 = await Canvas.loadImage(`./assets/cards/${playerDeck[1]}.png`);
            ctx.drawImage(playerCard2, 115, 50, 93, 135);

            var renderPlayerCards = [];
            cardAmount = cardAmount - 1;
            for (i = 2; i < playerStep; i++) {
                var position = 40 * i + 75;
                renderPlayerCards[i] = `./assets/cards/blank.png`;
                if (playerStep >= i) renderPlayerCards[i] = `./assets/cards/${playerDeck[i]}.png`;
                renderPlayerCards[i] = await Canvas.loadImage(renderPlayerCards[i]);
                ctx.drawImage(renderPlayerCards[i], position, 50, 93, 135);
                if (i >= cardAmount) break;
            }
            //Draw Dealer Cards
            const dealerCards1 = await Canvas.loadImage(`./assets/cards/${dealerDeck[0]}.png`);
            ctx.drawImage(dealerCards1, 75, 220, 93, 135);
            const dealerCards2 = await Canvas.loadImage(`./assets/cards/${dealerDeck[1]}.png`);
            ctx.drawImage(dealerCards2, 115, 220, 93, 135);

            var renderDealerCards = [];
            for (i = 2; i < dealerStep; i++) {
                var position = 40 * i + 75;
                renderDealerCards[i] = `./assets/cards/blank.png`;
                if (dealerSum >= 21) getDealerCards1 = `./assets/cards/${dealerDeck[0]}.png`;
                if (dealerStep > i) renderDealerCards[i] = `./assets/cards/${dealerDeck[i]}.png`;
                renderDealerCards[i] = await Canvas.loadImage(renderDealerCards[i]);
                ctx.drawImage(renderDealerCards[i], position, 220, 93, 135);
                if (i >= cardAmount) break;
            }
            const attachment = new Discord.MessageAttachment(canvas.toBuffer(), `blackjack.png`);


            if (result[2] == 'tie') {
                //nothing
                win = 0;
                client.db.addEco(SID, message.author, `xp`, client.db.prestigeXP(SID, message.author, 5));
                veto_stats(win, '+');
            }
            if (result[2] == 0) {
                //lost
                win = result[3];
                client.db.addEco(SID, message.author, `xp`, client.db.prestigeXP(SID, message.author, 5));
                client.db.subEco(SID, message.author, `money`, win);
                veto_stats(win, '-');
            }
            if (result[2] == 1) {
                //win
                win = result[3];
                client.db.addEco(SID, message.author, `xp`, client.db.prestigeXP(SID, message.author, 10));
                client.db.addEco(SID, message.author, `money`, win);
                veto_stats(win, '+');
            }

            var embedPost = new Discord.MessageEmbed()
                .setColor(result[5])
                .setAuthor(`${message.author.username} played blackjack`, message.author.avatarURL())
                //.addField(result[4], result[0])
                .setTitle(result[0])
                .attachFiles(attachment)
                .setImage('attachment://blackjack.png')
            message.channel.send(embedPost);

            client.db.setEco(SID, message.author, `blackjackMoney`, `0`);
        }
    }
}