module.exports = {
    name: "veto",
    cooldown: 3600,
    run (client, message, args, cooldownTime) {
        const Discord = require('discord.js');
        const number = Intl.NumberFormat();
        if (message.channel.type != 'dm') SID = message.guild.id;
        if (message.channel.type == 'dm') SID = 'dm';
        if (client.config.vetoEnable == 0) return message.channel.send(`Veto is disabled`);
        //Variables
        var money = 0;
        var option = args[0];
        if (option != null) option = option.toLowerCase();
        var games = ['blackjack', 'crash', 'dice', 'highlow', 'highslots', 'redblack', 'roulette', 'slots'];
        var veto_money = client.db.getEco(SID, message.author, `veto_money`);
        var veto_status = client.db.getEco(SID, message.author, `veto_status`);
        var veto_game = client.db.getEco(SID, message.author, 'veto_game');
        var veto_min_xp = client.config.vetoMinXP;
        var user_xp = client.db.getEco(SID, message.author, `xp`);
        var veto_amount = user_xp /100 * 10;
        veto_amount = veto_amount.toFixed(0);

        if (veto_status == '-') {
            money = veto_money / 100 * 90;
            money = money.toFixed(0);
            var veto_info1 = `You lost ${number.format(veto_money)}$ playing ` + '`' + `${veto_game}` + '`' + ``;
            var veto_info2 = `You'll get ${number.format(money)}$ back for ${number.format(veto_amount)} XP`;
        }
        if (veto_status == '+') {
            var veto_info1 = `You Won ${number.format(veto_money)}$ playing ` + '`' + `${veto_game}` + '`' + ``;
            var veto_info2 = `${number.format(veto_money)}$ will be taken for ${number.format(veto_amount)} XP`;
        }


        function veto_reset() {
            //veto
            client.db.setEco(SID, message.author, `veto_money`, `0`);
            client.db.setEco(SID, message.author, `veto_game`, `0`);
            client.db.setEco(SID, message.author, `veto_status`, `0`);
        }


        //Calculate XP to play
        if (user_xp < veto_min_xp * 10) return message.reply(`\nYou need ${number.format(veto_min_xp * 10)} XP or more to veto\nVeto costs 10% of your XP`);


        //show cooldown Time and last game
        if (option == 'info') {
            if (cooldownTime <= 60 && cooldownTime != 0) {
                if (veto_game != 0) return message.reply(`${cooldownTime} seconds remaining\n${veto_info1}\n${veto_info2}`);
                if (veto_game == 0) return message.reply(`\nYou can veto again in ${cooldownTime} seconds but there is nothing to gain for you`);
            }
            if (cooldownTime <= 3600 && cooldownTime != 0) {
                cooldownTime = cooldownTime / 60;
                if (veto_game != 0) return message.reply(`${cooldownTime.toFixed(1)} minutes remaining\n${veto_info1}\n${veto_info2}`);
                if (veto_game == 0) return message.reply(`\nYou can veto again ${cooldownTime.toFixed(1)} minutes but there is nothing to gain for you`);
            }
            if (cooldownTime <= 86400 && cooldownTime != 0) {
                cooldownTime = cooldownTime / 60 / 60;
                if (veto_game != 0) return message.reply(`${cooldownTime.toFixed(1)} hours remaining\n${veto_info1}\n${veto_info2}`);
                if (veto_game == 0) return message.reply(`\nYou can veto again in ${cooldownTime.toFixed(1)} hours but there is nothing to gain for you`);
            }
            if (cooldownTime > 86400 && cooldownTime != 0) {
                cooldownTime = cooldownTime / 60 / 60 / 24;
                if (veto_game != 0) return message.reply(`${cooldownTime.toFixed(1)} days remaining\n${veto_info1}\n${veto_info2}`);
                if (veto_game == 0) return message.reply(`\nYou can veto again in ${cooldownTime.toFixed(1)} days but there is nothing to gain for you`);
            }
            if (cooldownTime == 0 && veto_status != 0) {
                client.db.setEco(SID, message.author, `cooldown_veto`, `0`);
                message.reply(`You can veto again\n${veto_info1}\n${veto_info2}`);
                return;
            }
            if (cooldownTime == 0 && veto_status == 0) {
                client.db.setEco(SID, message.author, `cooldown_veto`, `0`);
                message.reply(`\nYou can veto again but there is nothing to gain for you`);
                return;
            }
            return;
        }


        //cooldown
        if (cooldownTime == 0 && veto_status == 0) {
            message.reply(`\nYou can veto again but there is nothing to gain for you`);
            return;
        }
        if (cooldownTime <= 60 && cooldownTime != 0) {
            if (veto_status == 0) return message.reply(`\nYou can veto again in ${cooldownTime} seconds but there is nothing to gain for you`);
            if (veto_status != 0) return message.reply(`${cooldownTime} seconds remaining\n${veto_info1}\n${veto_info2}`);
        }
        if (cooldownTime <= 3600 && cooldownTime != 0) {
            cooldownTime = cooldownTime / 60;
            if (veto_status == 0) return message.reply(`\nYou can veto again in ${cooldownTime.toFixed(1)} minutes but there is nothing to gain for you`);
            if (veto_status != 0) return message.reply(`${cooldownTime.toFixed(1)} minutes remaining\n${veto_info1}\n${veto_info2}`);
            return;
        }
        if (cooldownTime <= 86400 && cooldownTime != 0) {
            cooldownTime = cooldownTime / 60 / 60;
            if (veto_status == 0) return message.reply(`\nYou can veto again in ${cooldownTime.toFixed(1)} hours but there is nothing to gain for you`);
            if (veto_status != 0) return message.reply(`${cooldownTime.toFixed(1)} hours remaining\n${veto_info1}\n${veto_info2}`);
            return;
        } else
        if (cooldownTime > 86400 && cooldownTime != 0) {
            cooldownTime = cooldownTime / 60 / 60 / 24;
            if (veto_status == 0) return message.reply(`\nYou can veto again in ${cooldownTime.toFixed(1)} days but there is nothing to gain for you`);
            if (veto_status != 0) return message.reply(`${cooldownTime.toFixed(1)} days remaining\n${veto_info1}\n${veto_info2}`);
            return;
        }       
        if (veto_game == '0') {
            message.reply(`\nNothing to veto`);
            client.db.setEco(SID, message.author, `cooldown_veto`, `0`);
            return;
        }
        if (games.includes(veto_game) == false) {
            message.reply("\nYou can't veto `" + veto_game + '`');
            client.db.setEco(SID, message.author, `cooldown_veto`, `0`);
            return;
        }


        //Veto
        if ((veto_money != null || veto_status != null || veto_money != '0' || veto_status != '0') && user_xp >= veto_amount && user_xp >= veto_min_xp) {
            client.db.subEco(SID, message.author, `xp`, veto_amount);

            if (veto_status == '-') {
                if (user_xp < veto_amount) return;
                client.db.addEco(SID, message.author, 'money', money);
                message.reply('\nYour last game, `' + veto_game + '`' + `, has been undone\n${number.format(money)}$ have been added to your balance`);
                veto_reset();
                return;
            }
            if (veto_status == '+') {
                if (user_xp < veto_amount) return;
                client.db.subEco(SID, message.author, 'money', veto_money);
                message.reply('\nYour last game, `' + veto_game + '`' + `, has been undone\n${number.format(veto_money)}$ have been taken from your balance`);
                veto_reset();
                return;
            }
        }

        if (user_xp < veto_amount) message.reply(`You need ${number.format(veto_amount)}XP to veto`);
        
        message.reply(`\nStart gambling and you can veto a game a day in exchange for XP`);
        return;


    }
}