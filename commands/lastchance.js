module.exports = {
    name: "lastChance",
    cooldown: 0,
    run (client, message, args, cooldownTime) {
        var number = Intl.NumberFormat();
        if (message.channel.type != 'dm') SID = message.guild.id;
        if (message.channel.type == 'dm') SID = 'dm';
        var balance = client.db.getEco(SID, message.author, `money`);
        var lowerLimit = client.config.lowerLimit;

        //veto function
        //set name of current game
        client.db.setEco(SID, message.author, `veto_game`, client.commands.get(message.content.slice(prefix.length).trim().split(/ +/g).shift().toLowerCase()).name);
        //set win/loss
        //set +/- depending on the outcome of the game
        function veto_stats(win, status) {
            //veto
            client.db.setEco(SID, message.author, `veto_money`, win);
            client.db.setEco(SID, message.author, `veto_status`, status);
        }
        
        if (balance == 0) return message.reply(`\nYou have 0$`);
        if (balance >= lowerLimit) {
            message.reply(`\nYou got ${number.format(balance)}$ left`);
            return;
        }
        var chance = ["0", "1"];
        chance = chance[Math.floor(Math.random() * chance.length)];
        if (chance == 0) {
            client.db.subEco(SID, message.author, `money`, balance);
            message.reply(`\nNo luck this time`);
            return;
        }
        if (chance == 1){
            var refill = lowerLimit - balance;
            client.db.addEco(SID, message.author, `money`, refill);
            message.reply(`\nYour money was refilled to ${client.config.lowerLimit}$`);
            return;
        }
    }
}