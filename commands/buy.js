module.exports = {
    name: "exchange",
    cooldown: 30,
    run (client, message, args, cooldownTime) {
        if (cooldownTime != 0) return message.reply(`\nplease wait ${cooldownTime} seconds to exchange again`);
        if (message.channel.type != 'dm') SID = message.guild.id;
        if (message.channel.type == 'dm') SID = 'dm';
        var xp = client.db.getEco(SID, message.author, `xp`);
        var balance = client.db.getEco(SID, message.author, `money`);
        var option = args[0];
        if (option != null) option = option.toLowerCase();


        //veto function
        //set name of current game
        client.db.setEco(SID, message.author, `veto_game`, client.commands.get(message.content.slice(prefix.length).trim().split(/ +/g).shift().toLowerCase()).name);
        //set win/loss
        //set +/- depending on the outcome of the game
        function veto_stats(win, status) {
            //veto
            client.db.setEco(SID, message.author, `veto_money`, win);
            client.db.setEco(SID, message.author, `veto_status`, status);
        }
        

        if (client.db.getEco(SID, message.author, `luckywheel_freespin`) == 1) return message.channel.send(`You already got a freespin`);

        if (option == `xp`) {
            if (xp >= 250) {
                client.db.subEco(SID, message.author, `xp`, 250);
                client.db.setEco(SID, message.author, 'luckywheel_freespin', 1);
                message.reply(`\nYou've bought a spin for 250XP\ngood luck!\n`);
                return
            } return message.reply(`\nYou dont have enough XP\nXP: ${xp}XP`);
        }


        if (option == `$`) {
            if (balance >= 100000) {
                client.db.subEco(SID, message.author, `money`, 100000)
                client.db.setEco(SID, message.author, 'luckywheel_freespin', 1);
                message.reply(`\nYou've bought a spin for 100.000$\ngood luck!\n`);
                return
            } return message.reply(`\nYou dont have enough money\nBalance: ${balance}`);
        }

        return message.reply('\n`buy xp` to buy a Spin for 250XP or\n`buy $` to buy a Spin for 100.000$')
    }
}
