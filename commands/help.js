exports.run = (client, message, args) => {
    const Discord = require('discord.js');
    const number = Intl.NumberFormat();
    cmd = args[0];
    if (cmd != null) cmd = cmd.toLowerCase();
    //Variables


    if (cmd == "$") {
        const help = new Discord.MessageEmbed()
            .setColor(0x5886ce)
            .setAuthor(`Hello, I'm lain`, client.user.avatarURL())
            .addField(`Check your balance`, 'Do `$ @user` to check the balance of another User')
            .attachFiles(`./assets/avatar_64.png`)
            .setFooter(`written by ${client.package.author}`, `attachment://avatar_64.png`)
        message.channel.send(help);
        return;
    } else
    if (cmd == "blackjack") {
        const help = new Discord.MessageEmbed()
            .setColor(0x5886ce)
            .setAuthor(`Hello, I'm lain`, client.user.avatarURL())
            .addField(`Play Blackjack`, 'Do `blackjack 5000` to start a new game\nDraw cards by clicking on the reactions')
            .attachFiles(`./assets/avatar_64.png`)
            .setFooter(`written by ${client.package.author}`, `attachment://avatar_64.png`)
        message.channel.send(help);
        return;
    } else
    if (cmd == "buy") {
        const help = new Discord.MessageEmbed()
            .setColor(0x5886ce)
            .setAuthor(`Hello, I'm lain`, client.user.avatarURL())
            .addField(`Buy a Spin on the Lucky Wheel`, 'Do `buy xp` to exchange 250XP for a freespin\nDo `buy $` to exchange 100.000$ for a freespin')
            .attachFiles(`./assets/avatar_64.png`)
            .setFooter(`written by ${client.package.author}`, `attachment://avatar_64.png`)
        message.channel.send(help);
        return;
    } else
    if (cmd == "coinflip") {
        const help = new Discord.MessageEmbed()
            .setColor(0x5886ce)
            .setAuthor(`Hello, I'm lain`, client.user.avatarURL())
            .addField(`Battle with another player in coinflip`, 'Do `coinflip money @user` to initiate a Battle\nAccept Battles with `coinflip heads/tails @user`')
            .attachFiles(`./assets/avatar_64.png`)
            .setFooter(`written by ${client.package.author}`, `attachment://avatar_64.png`)
        message.channel.send(help);
        return;
    } else
    if (cmd == "crash") {
        const help = new Discord.MessageEmbed()
            .setColor(0x5886ce)
            .setAuthor(`Hello, I'm lain`, client.user.avatarURL())
            .addField(`Beat the timer`, 'Do `crash 5000` to start playing\nStop the timer before it crashes and multiply your money')
            .attachFiles(`./assets/avatar_64.png`)
            .setFooter(`written by ${client.package.author}`, `attachment://avatar_64.png`)
        message.channel.send(help);
        return;
    } else
    if (cmd == "data") {
        const help = new Discord.MessageEmbed()
            .setColor(0x5886ce)
            .setAuthor(`Hello, I'm lain`, client.user.avatarURL())
            //.setDescription(`Interact with the database`)
            .addField(`Interact with the database`, 'syntax `data command entry value @user`\n@user is optional')
            .addField('User commands', '`get`\nget the value of any database entry\n`lb`\ncreate a leaderboard from any database entry')
            .addField('Admin commands', '`set`\nchange the value of any database entry\n`mk`\ncreate a new database entry\n`add`, `sub`\nadd or subtract values from any database entry')
            .attachFiles(`./assets/avatar_64.png`)
            .setFooter(`written by ${client.package.author}`, `attachment://avatar_64.png`)
        message.channel.send(help);
        return;
    } else
    if (cmd == "daily") {
        const help = new Discord.MessageEmbed()
            .setColor(0x5886ce)
            .setAuthor(`Hello, I'm lain`, client.user.avatarURL())
            .addField(`Get your daily 5.000$`, 'You can run the `daily` Command every 24h')
            .attachFiles(`./assets/avatar_64.png`)
            .setFooter(`written by ${client.package.author}`, `attachment://avatar_64.png`)
        message.channel.send(help);
        return;
    } else
    if (cmd == "dice") {
        const help = new Discord.MessageEmbed()
            .setColor(0x5886ce)
            .setAuthor(`Hello, I'm lain`, client.user.avatarURL())
            .addField(`Roll the Dice and sixfold your money`, 'Do `dice eyes money` to play\nEyes is the side between 1 and 6 you wanna bet on')
            .attachFiles(`./assets/avatar_64.png`)
            .setFooter(`written by ${client.package.author}`, `attachment://avatar_64.png`)
        message.channel.send(help);
        return;
    } else
    if (cmd == "highlow") {
        const help = new Discord.MessageEmbed()
            .setColor(0x5886ce)
            .setAuthor(`Hello, I'm lain`, client.user.avatarURL())
            .addField(`Play Highlow`, 'Cards below 5 count as low\nCards 5 and above count as high\nDo `highlow high/low 500` to play')
            .attachFiles(`./assets/avatar_64.png`)
            .setFooter(`written by ${client.package.author}`, `attachment://avatar_64.png`)
        message.channel.send(help);
        return;
    } else
    if (cmd == "highslots") {
        const help = new Discord.MessageEmbed()
            .setColor(0x5886ce)
            .setAuthor(`Hello, I'm lain`, client.user.avatarURL())
            .addField(`Play Highslots with up to 500.000$`, 'You can play on up to ' + client.config.maxHighslots + ' multiple Slotmachines with `highslots money number`')
            .attachFiles(`./assets/avatar_64.png`)
            .setFooter(`written by ${client.package.author}`, `attachment://avatar_64.png`)
        message.channel.send(help);
        return;
    } else
    if (cmd == "htop") {
        const help = new Discord.MessageEmbed()
            .setColor(0x5886ce)
            .setAuthor(`Hello, I'm lain`, client.user.avatarURL())
            .addField(`See the all-time highscores`, 'Do `htop number` to expand the leaderboard')
            .addField('`htop m`', 'Get mobile a compatible leaderboardlayout\nthis can also be expanded with `htop m number`')
            .attachFiles(`./assets/avatar_64.png`)
            .setFooter(`written by ${client.package.author}`, `attachment://avatar_64.png`)
        message.channel.send(help);
        return;
    } else
    if (cmd == "insurance") {
        const help = new Discord.MessageEmbed()
            .setColor(0x5886ce)
            .setAuthor(`Hello, I'm lain`, client.user.avatarURL())
            .addField(`Get your money back`, "Get your insurance back with `insurance`\nYou can buy an insurance with `insurance buy 5000`")
            .setFooter(`written by momo™`, `https://cdn.discordapp.com/avatars/352153726116560898/ba5fb13bf1afb0763b89cd35c9c5264e.png`)
        message.channel.send(help);
        return;
    } else
    if (cmd == "lastchance") {
        const help = new Discord.MessageEmbed()
            .setColor(0x5886ce)
            .setAuthor(`Hello, I'm lain`, client.user.avatarURL())
            .addField(`Your last Chance if you fall below 100$`, 'Gives you a 50/50 chance to get refilled to 100$ or lose it all')
            .attachFiles(`./assets/avatar_64.png`)
            .setFooter(`written by ${client.package.author}`, `attachment://avatar_64.png`)
        message.channel.send(help);
        return;
    } else
    if (cmd == "lotto") {
        const help = new Discord.MessageEmbed()
            .setColor(0x5886ce)
            .setAuthor(`Hello, I'm lain`, client.user.avatarURL())
            .addField('Tipping costs 1.000$', 'Tip 5 Numbers between 1 and 50\n+ Super between 1 and 10\n`lotto 1 2 3 4 5 6`\n\n1 correct Tip = 1.000$\n2 correct Tips = 10.000$\n3 correct Tips = 100.000$\n4 correct Tips = 1.000.000$\n5 correct Tips = 10.000.000$\nSuper gives x' + client.config.lottoMultiplicator + ' multiplicator')
            .attachFiles(`./assets/avatar_64.png`)
            .setFooter(`written by ${client.package.author}`, `attachment://avatar_64.png`)
        message.channel.send(help);
        return;
    } else
    if (cmd == "prestige") {
        const help = new Discord.MessageEmbed()
            .setColor(0x5886ce)
            .setAuthor(`Hello, I'm lain`, client.user.avatarURL())
            .addField('Go prestige!', 'Earn more XP, Money and level up your Badges')
            .addField('`prestige $`', `Get +${client.config.prestige.moneyX}% more money\nRank up to level ${client.config.prestige.moneyCap} and get +${parseInt(client.config.prestige.moneyX)*parseInt(client.config.prestige.moneyCap)}% $$$`)
            .addField('`prestige xp`', `Get +${client.config.prestige.xpX}% more experience\nRank up to level ${client.config.prestige.xpCap} and get +${parseInt(client.config.prestige.xpX)*parseInt(client.config.prestige.xpCap)}% XP`)
            .addField(`You will be reset to`, `${number.format(client.config.prestige.moneyReset)}$\nor ${number.format(client.config.prestige.xpReset)} XP`)
            .attachFiles(`./assets/avatar_64.png`)
            .setFooter(`written by ${client.package.author}`, `attachment://avatar_64.png`)
        message.channel.send(help);
        return;
    } else
    if (cmd == "raffle") {
        const help = new Discord.MessageEmbed()
            .setColor(0x5886ce)
            .setAuthor(`Hello, I'm lain`, client.user.avatarURL())
            .addField(`Buy a raffle ticket`, 'A ticket costs 500$\nMain Price is 1.000.000$')
            .setFooter(`written by momo™`, `https://cdn.discordapp.com/avatars/352153726116560898/ba5fb13bf1afb0763b89cd35c9c5264e.png`)
        message.channel.send(help);
        return;
    } else
    if (cmd == "random") {
        const help = new Discord.MessageEmbed()
            .setColor(0x5886ce)
            .setAuthor(`Hello, I'm lain`, client.user.avatarURL())
            .addField(`Roll the dice or draw a card`, '`random dice` to get a random roll or\n`random card` to draw a random card')
            .setFooter(`written by ${client.package.author}`, `attachment://avatar_64.png`)
        message.channel.send(help);
        return;
    } else
    if (cmd == "redblack") {
        const help = new Discord.MessageEmbed()
            .setColor(0x5886ce)
            .setAuthor(`Hello, I'm lain`, client.user.avatarURL())
            .addField(`Play Redblack`, '`redblack red/black money` to draw three cards\nIf two cards match your color you win\nIf three cards match, you double your win')
            .attachFiles(`./assets/avatar_64.png`)
            .setFooter(`written by ${client.package.author}`, `attachment://avatar_64.png`)
        message.channel.send(help);
        return;
    } else
    if (cmd == "roulette") {
        if (client.config.europeanRoulette == 1) var mode = 'european';
        if (client.config.europeanRoulette != 1) var mode = 'american';
        const help = new Discord.MessageEmbed()
            .setColor(0x5886ce)
            .setAuthor(`Hello, I'm lain`, client.user.avatarURL())
            .addField(`Go all in with your money and play roulette`, 'Do `roulette color/number money` to play \nYou can play with any color or number' + `\n\nRoulette is currently set to ${mode} roulette`)
            .attachFiles(`./assets/avatar_64.png`)
            .setFooter(`written by ${client.package.author}`, `attachment://avatar_64.png`)
        message.channel.send(help);
        return;
    } else
    if (cmd == "send") {
        const help = new Discord.MessageEmbed()
            .setColor(0x5886ce)
            .setAuthor(`Hello, I'm lain`, client.user.avatarURL())
            .addField(`Send money/XP to another user`, '`send 500 @user` sends money to a user\n`send xp 500 @user` sends XP to a user')
            .attachFiles(`./assets/avatar_64.png`)
            .setFooter(`written by ${client.package.author}`, `attachment://avatar_64.png`)
        message.channel.send(help);
        return;
    } else
    if (cmd == "slots") {
        const help = new Discord.MessageEmbed()
            .setColor(0x5886ce)
            .setAuthor(`Hello, I'm lain`, client.user.avatarURL())
            .addField(`Bet 250$ on my Slotmachine`, 'You can play on up to ' + client.config.maxSlots + ' multiple Slotmachines with `slots number`')
            .attachFiles(`./assets/avatar_64.png`)
            .setFooter(`written by ${client.package.author}`, `attachment://avatar_64.png`)
        message.channel.send(help);
        return;
    } else
    if (cmd == "spin") {
        const help = new Discord.MessageEmbed()
            .setColor(0x5886ce)
            .setAuthor(`Hello, I'm lain`, client.user.avatarURL())
            .addField(`Spin the Lucky Wheel`, 'Win between 100$ and 1.000.000$\nRespin gives x2 Multipilcator on your next Spin\nYou can spin the lucky wheel every 24h')
            .attachFiles(`./assets/avatar_64.png`)
            .setFooter(`written by ${client.package.author}`, `attachment://avatar_64.png`)
        message.channel.send(help);
        return;
    } else
    if (cmd == "stats") {
        const help = new Discord.MessageEmbed()
            .setColor(0x5886ce)
            .setAuthor(`Hello, I'm lain`, client.user.avatarURL())
            .addField('Check your progress with `stats`', 'Display your current Level, XP, Balance and Highscore\n\n`stats image www.site.com/img.png`\nSet a background Image for your Statistics card\nImage resolution should be 700x250px\nImages must be in a `.png`, `.jpg` or `.jpeg` Format\n\n`stats text color1 color2`\nChange the text Color\ncolor1 is the text Color\ncolor2 is the text outline\n\n`stats xpbar color`\nChange the Color of the progress bar\n\nAll Colors must be given as Hex Values\n`stats text ffffff 000000`\n`stats xpbar 719b1f`\n\n`stats defaults`\nRestore the default Settings')
            .attachFiles(`./assets/avatar_64.png`)
            .setFooter(`written by ${client.package.author}`, `attachment://avatar_64.png`)
        message.channel.send(help);
        return;
    } else
    if (cmd == "top") {
        const help = new Discord.MessageEmbed()
            .setColor(0x5886ce)
            .setAuthor(`Hello, I'm lain`, client.user.avatarURL())
            .addField(`See who's ballin right now`, 'Do `top number` to expand the leaderboard')
            .addField('`top m`', 'Get mobile a compatible leaderboardlayout\nthis can also be expanded with `top m number`')
            .attachFiles(`./assets/avatar_64.png`)
            .setFooter(`written by ${client.package.author}`, `attachment://avatar_64.png`)
        message.channel.send(help);
        return;
    } else
    if (cmd == "veto") {
        var vetoMinXP = client.config.vetoMinXP * 10;
        vetoMinXP = vetoMinXP.toFixed(0);

        const help = new Discord.MessageEmbed()
            .setColor(0x5886ce)
            .setAuthor(`Hello, I'm lain`, client.user.avatarURL())
            .addField(`Undo the last game you played`, 'Do `veto` to get your money back\nThis will also take money when you won\n')
            .addField(`How it works`, `Vetoing a game costs 10% of your total XP\nYou'll get back 90% the money from your last game\nYou need at least ` + number.format(vetoMinXP) + ` XP to use veto`)
            .addField('It works with:', '`dice`, `highlow`, `slots`, `highslots`, `lotto`\n`redblack`, `roulette`, `blackjack`, `raffle`\n\n`veto info` will give some usefull information')
            .attachFiles(`./assets/avatar_64.png`)
            .setFooter(`written by ${client.package.author}`, `attachment://avatar_64.png`)
        message.channel.send(help);
        return;
    } else
    if (cmd == "weekly") {
        const help = new Discord.MessageEmbed()
            .setColor(0x5886ce)
            .setAuthor(`Hello, I'm lain`, client.user.avatarURL())
            .addField(`Get your daily 50.000$`, 'You can run the `weekly` Command every 7d')
            .attachFiles(`./assets/avatar_64.png`)
            .setFooter(`written by ${client.package.author}`, `attachment://avatar_64.png`)
        message.channel.send(help);
        return;
    } else
    if (cmd == "xtop") {
        const help = new Discord.MessageEmbed()
            .setColor(0x5886ce)
            .setAuthor(`Hello, I'm lain`, client.user.avatarURL())
            .addField(`See who's farming the most`, 'Do `top number` to expand the leaderboard')
            .addField('`xtop m`', 'Get mobile a compatible leaderboardlayout\nthis can also be expanded with `xtop m number`')
            .attachFiles(`./assets/avatar_64.png`)
            .setFooter(`written by ${client.package.author}`, `attachment://avatar_64.png`)
        message.channel.send(help);
        return;
    } else
        var help = new Discord.MessageEmbed()
            .setColor(0x5886ce)
            .setAuthor(`Hello, I'm lain`, client.user.avatarURL())
            .setDescription(`Here are some things you can do with me`)
            .addField(`Games`, '`roulette`, `slots`, `highslots`, `spin`\n`highlow`, `redblack`, `lotto`, `raffle`\n`blackjack`, `crash`, `dice`, `coinflip`')
            .addField(`Miscellaneous`, '`daily`, `weekly`, `veto`, `send`, `buy`\n`stats`, `top`, `xtop`, `htop`, `random`\n`lastchance`, `insurance`, `mstats`\n`prestige`, `data`')
            .addField(`Need help?`, '`help commandName`')
            .addField(`Glücksspiel kann süchtig machen.`, `Infos und Hilfe unter [www.BZgA.de](https://gitlab.com/Alice1337/lain-bot)`)
            .attachFiles(`./assets/navi.png`)
            .setFooter(`lain v${client.package.version } • written by ${client.package.author} & momo™`, `attachment://navi.png`)
            //.setTimestamp(new Date)
        message.channel.send(help);
        return;
}
