exports.run = (client, message, args) => {
    //check privileges
    var priv = client.config.privileges;
    if (message.author.id !== priv.adminID) return message.channel.send(`You have no permission to do that`);


    var number = Intl.NumberFormat();
    if (message.channel.type != 'dm') SID = message.guild.id;
    if (message.channel.type == 'dm') SID = 'dm';
    if (message.mentions.members != null) var name = message.mentions.members.first();
    var option = args[0];
    var money = args[0];
    if (money != null) money = money.toLowerCase();
    var xp = args[1];
    if (xp != null) xp = xp.toLowerCase();
    

    //give XP
    if (money == 'xp') {
        if (isNaN(xp)) return message.reply(`please specify how much xp to give`);
        if (name == null) {
            client.db.addEco(SID, message.author, `xp`, xp);
            var xp = number.format(xp);
            message.reply(`You have been credited with ${xp} XP, have fun`);
            return;
        } else
        if (name != null) {
            var name = message.mentions.members.first().user.username;
            let user = message.mentions.members.first().user;
            client.db.addEco(SID, user, `xp`, xp)
            var xp = number.format(xp);
            message.channel.send(`Hey ${name}, you have been given ${xp} XP`);
            return;
        }
    }

    //give money
    if (isNaN(money)) return message.reply(`please specify how much money to give`);

    if (name == null) {
        client.db.addEco(SID, message.author, `money`, money);
        var money = number.format(money);
        message.reply(`You have been credited with ${money}$, have fun`);
        return;
    } else
    if (name != null) {
        var name = message.mentions.members.first().user.username;
        let user = message.mentions.members.first().user;
        client.db.addEco(SID, user, `money`, money)
        var money = number.format(money);
        message.channel.send(`Hey ${name}, you have been given ${money}$`);
        return;
    }
}