module.exports = {
    name: "casino",
    cooldown: 0,
    run (client, message, args, cooldownTime) {
        if (message.channel.type != 'dm') SID = message.guild.id;
        if (message.channel.type == 'dm') SID = 'dm';
        var option = args[0];
        if (option != null) option = option.toLowerCase();

        if (client.config.botSettings.requireCasinoRole == 0) return;

        if (message.mentions.members.first() != null && message.author.id === client.config.privileges.adminID) {
            var member = message.mentions.members.first();
            var user = message.mentions.members.first().user.username;
            var ID = message.mentions.members.first().user;
            var role = message.guild.roles.cache.find(role => role.name === "Casino");

            if (option == 'kick') {                
                member.roles.remove(role).catch(console.error);
                return message.channel.send(`${member} has been kicked out of the Casino`);
            }
            if (option == 'ban') {
                member.roles.remove(role).catch(console.error);
                client.db.setEco(SID, ID, `casino_ban`, `1`)
                return message.channel.send(`${member} has been banned from the Casino`);
            }
            if (option == 'unban') {
                member.roles.remove(role).catch(console.error);
                client.db.setEco(SID, ID, `casino_ban`, `0`)
                return message.channel.send(`${member} has been unbanned from the Casino`);
            }
            member.roles.add(role).catch(console.error);
            return message.channel.send(`Hey ${member}, you have been invited to the Casino\nGet started with` + '`help`');
        }

        
        if (option == null) {
            if (message.member.roles.cache.some(r=>["Casino"].includes(r.name))) return message.reply("\nYou'r already in the Casino\nYou can leave with `casino leave`");
            var role = message.guild.roles.cache.find(role => role.name === "Casino");
            message.member.roles.add(role).catch(console.error);
            message.reply('\nWelcome to the Casino\n`help` will show you around');
            return;
        }

        if (option == 'leave') {
            var role = message.guild.roles.cache.find(role => role.name === "Casino");
            message.member.roles.remove(role).catch(console.error);
            message.reply('\nYou left the Casino but your progress will be saved');
            return;
        }
    }
}
