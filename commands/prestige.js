module.exports = {
    name: "prestige",
    cooldown: 0,
	async run (client, message, args, cooldownTime) {
        //Cooldown
        if (cooldownTime != 0) return message.reply(`\nplease wait ${cooldownTime} seconds to play again`);

        //Constants
        const Discord = require('discord.js');
        const Canvas = require('canvas');
        const number = Intl.NumberFormat();
        if (message.channel.type != 'dm') SID = message.guild.id;
        if (message.channel.type == 'dm') SID = 'dm';
        if (message.mentions.members != null) var user = message.mentions.members.first();

        //Variables
        var option = args[0];
        if (option != null) option = option.toLowerCase();
        var balance = client.db.getEco(SID, message.author, `money`);
        var xp = client.db.getEco(SID, message.author, `xp`);
        //Get config settings
        var moneyNeeded = client.config.prestige.moneyNeeded;                           //Money needed for prestige
        var moneyReset = client.config.prestige.moneyReset;                             //Money after prestige
        var moneyX = client.config.prestige.moneyX;                                     //Multiplicator value for money
        var prestigemoney = client.db.getEco(SID, message.author, `prestigemoney`);     //Current prestige lvl for money
        var xpNeeded = client.config.prestige.xpNeeded;                                 //XP needed for prestige
        var xpReset = client.config.prestige.xpReset;                                   //XP after prestige
        var xpX = client.config.prestige.xpX;                                           //Multiplicator value for XP
        var prestigexp = client.db.getEco(SID, message.author, `prestigexp`);           //Current prestige lvl for XP


        //Check XP and money
        if (option == '$' && balance < moneyNeeded) return message.reply(`\nYou need at least ${number.format(moneyNeeded)}$ to go prestige`);
        if (option == 'xp' && xp < xpNeeded) return message.reply(`\nYou need at least ${number.format(xpNeeded)} XP to go prestige`);
        if (option != '$' && option != 'xp') return message.reply(`\ngo prestige with either $ or xp`);
        if (option == null) return message.reply(`\ngo prestige with either $ or xp`);


        //Check Levelcap
        if (prestigemoney >= client.config.prestige.moneyCap && option == '$') return message.channel.send(`You've already reached the highest prestige one can get!`);
        if (prestigexp >= client.config.prestige.xpCap && option == 'xp') return message.channel.send(`You've already reached the highest prestige one can get!`);


        async function embedPrestige() {
            //set variables
            var lvl;
            var img;
            var mode;
            var x;
            if (option == '$') {
                lvl = client.db.getEco(SID, message.author, `prestigemoney`);
                img = await Canvas.loadImage(`./assets/prestige/money/${lvl}.png`);
                mode = '$';
                x = moneyX;
            }
            if (option == 'xp') {
                lvl = client.db.getEco(SID, message.author, `prestigexp`);
                img = await Canvas.loadImage(`./assets/prestige/xp/${lvl}.png`);
                mode = 'XP';
                x = xpX;
            }
            
            //get embedColor
            var color = 0x141414  
            if (lvl <= 9) color = 0xc99400;         //yellow
            if (lvl == 10) color = 0x9059cf;        //purple

            //canvas
            const canvas = Canvas.createCanvas(512, 512);
            const ctx = canvas.getContext('2d');
            const background = await Canvas.loadImage('./assets/background.png');
            const lainchan = await Canvas.loadImage('./assets/lainchan.png');
            ctx.drawImage(background, 0, 0, canvas.width, canvas.height);
            ctx.drawImage(img, 0, 0, canvas.width, canvas.height);
            //Game Text
            var fontSizeThin = 600;
            var fontSizeBold = 600;
            ctx.textAlign = 'center';
            ctx.font = `${fontSizeThin} 22px sans-serif`;
            ctx.strokeStyle = '#393939';
            ctx.strokeText(`P   R   E   S   T   I   G   E`, 256, 110);
            ctx.fillStyle = '#787878';
            ctx.fillText(`P   R   E   S   T   I   G   E`, 256, 110);
            ctx.textAlign = 'right';
            ctx.drawImage(lainchan, 112, 30);
            const attachment = new Discord.MessageAttachment(canvas.toBuffer(), `prestige.png`);
 
            //embed
            const embedPrestige = new Discord.MessageEmbed()
                .setColor(color)
                .setAuthor(`${message.author.username} is now Prestige ${lvl}`, message.author.avatarURL())
                .addField(`You've unlocked +${x*lvl}% ${mode} in all games`, `Your stats have been reset`)
                .attachFiles(attachment)
                .setImage('attachment://prestige.png')
            message.channel.send(embedPrestige);
            return;
        }
        

        //embedPost
        var lvl;
        var mode;
        var x;
        var msg;
        var reset;
        if (option == '$') {
            lvl = parseInt(prestigemoney)+1;
            mode = '$';
            x = moneyX;
            msg = 'money';
            reset = moneyReset;
            msg2 = '\nand your insurance will be voided';
        }
        if (option == 'xp') {
            lvl = parseInt(prestigexp)+1;
            mode = 'XP';
            x = xpX;
            msg = 'experience';
            reset = xpReset;
            msg2 = '';
        }

        //Confirm prestige levelup
        const embedPost = new Discord.MessageEmbed()
            .setColor(0xc99400)
            .setAuthor(`Do you wanna go prestige?`, message.author.avatarURL())
            .addField(`You will unlock +${x*lvl}% ${mode} in all games`, `You will start over with ${number.format(reset)} ${mode}${msg2}`)
        const reactionMessage = await message.channel.send(embedPost);
        await reactionMessage.react('👍'); //Draw
        await reactionMessage.react('👎'); //Hold

        const filter = (reaction, user) => {
            return ['👍', '👎'].includes(reaction.emoji.name) && user.id === message.author.id;
        };

        await reactionMessage.awaitReactions(filter, { max: 1, time: 25000, errors: ['time'] })
        .then(collected => {
            const reaction = collected.first();
            
            if (reaction.emoji.name === '👍') {
                if (option == '$') {
                    client.db.addEco(SID, message.author, `prestigemoney`, 1);
                    client.db.setEco(SID, message.author, `money`, moneyReset);
                    client.db.setEco(SID, message.author, `insurance`, 0);
                    reactionMessage.delete();
                    embedPrestige();
                    return;
                }
        
                if (option == 'xp') {
                    client.db.addEco(SID, message.author, `prestigexp`, 1);;
                    client.db.setEco(SID, message.author, `xp`, xpReset);
                    reactionMessage.delete();
                    embedPrestige();
                    return;
                }
            }

            if (reaction.emoji.name === '👎') {
                option = 0;
                return;
            }
        })
        .catch(collected => {
            option = 0;
            return;
        });
        
    }
}

/*
Hex colors
    Red     0xa82418
    Black   0x141414
    Green   0x719b1f
    Blue    0x5886ce
    Yellow  0xc99400
*/