exports.run = (client, message, args) => {
    //check privileges
    var priv = client.config.privileges;
    if (message.author.id !== priv.adminID) return message.channel.send(`You have no permission to do that`);


    var number = Intl.NumberFormat();
    if (message.channel.type != 'dm') SID = message.guild.id;
    if (message.channel.type == 'dm') SID = 'dm';
    if (message.mentions.members != null) var name = message.mentions.members.first();
    var money = args[0];
    if (money != null) money = money.toLowerCase();
    var xp = args[1];
    if (xp != null) xp = xp.toLowerCase();


    if (money == 'xp') {
        if (isNaN(xp)) return message.reply(`please specify how much xp to take`);
    
        if (name == null) {
            client.db.subEco(SID, message.author, `xp`, xp);
            var xp = number.format(xp);
            message.reply(`I have taken ${xp} XP from you`);
            return;
        } else
        if (name != null) {
            let user = message.mentions.members.first().user;
            client.db.subEco(SID, user, `xp`, xp);
            var xp = number.format(xp);
            message.channel.send(`I have taken ${xp} XP from ${name}`);
            return;
        }
    }
    
    
    if (isNaN(money)) return message.reply(`please specify how much money to take`);
    
    if (name == null) {
        client.db.subEco(SID, message.author, `money`, money);
        var money = number.format(money);
        message.reply(`I have taken ${money}$ from you`);
        return;
    } else
    if (name != null) {
        let user = message.mentions.members.first().user;
        client.db.subEco(SID, user, `money`, money);
        var money = number.format(money);
        message.channel.send(`I have taken ${money}$ from ${name}`);
        return;
    }
}
