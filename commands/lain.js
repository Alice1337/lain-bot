module.exports = {
    name: "template",
    cooldown: 0,
	async run (client, message, args, cooldownTime) {
        //Constants
        const Discord = require('discord.js');
        const Canvas = require('canvas');
        const number = Intl.NumberFormat();

        //Variables
        var command = args[0];
        var user = args[1];
        if (command != null) command = command.toLowerCase();


        if (command == null) {
            message.channel.send("I'm here");
            return;

        }

        if (command != null && user == null) {
            message.channel.send('please specify a user');
            return;
        }
        
        if (command == 'mute' && user != null) {
            message.channel.send(user + ' has been muted');
            return;
        }
        
        if (command == 'kick' && user != null) {
            message.channel.send(user + ' has been kicked');
            return;
        }
        
        if (command == 'ban' && user != null) {
            message.channel.send(user + ' has been banned');
            return;
        }
    }
}

/*
Hex colors
    Red     0xa82418
    Black   0x141414
    Green   0x719b1f
    Blue    0x5886ce
    Purple  0x9059cf
    Yellow  0xc99400

Prestige Win
    win = client.db.prestigeMoney(SID, message.author, win);
Prestige XP
    client.db.addEco(SID, message.author, `xp`, client.db.prestigeXP(SID, message.author, 10));
*/