module.exports = {
    name: "roulette",
    cooldown: 3,
    run (client, message, args, cooldownTime) {
        if (cooldownTime != 0) return message.reply(`\nplease wait ${cooldownTime} seconds to play again`);
        const Discord = require('discord.js');
        var number = Intl.NumberFormat();
        if (message.channel.type != 'dm') SID = message.guild.id;
        if (message.channel.type == 'dm') SID = 'dm';
        var balance = client.db.getEco(SID, message.author, `money`);
        var user = message.author.username;

        var red = ["1.png", "3.png", "5.png", "7.png", "9.png", "12.png", "14.png", "16.png", "18.png", "19.png", "21.png", "23.png", "25.png", "27.png", "30.png", "32.png", "34.png", "36.png"];
        var black = ["2.png", "4.png", "6.png", "8.png", "10.png", "11.png", "13.png", "15.png", "17.png", "20.png", "22.png", "24.png", "26.png", "28.png", "29.png", "31.png", "33.png", "35.png"];
        if (client.config.europeanRoulette == 1) {
            var symbol = ["0.png", "1.png", "2.png", "3.png", "4.png", "5.png", "6.png", "7.png", "8.png", "9.png", "10.png", "11.png", "12.png", "13.png", "14.png", "15.png", "16.png", "17.png", "18.png", "19.png", "20.png", "21.png", "22.png", "23.png", "24.png", "25.png", "26.png", "27.png", "28.png", "29.png", "30.png", "31.png", "32.png", "33.png", "34.png", "35.png", "36.png"];
            var green = ["0.png"];
        }
        if (client.config.europeanRoulette != 1) {
            var symbol = ["0.png", "1.png", "2.png", "3.png", "4.png", "5.png", "6.png", "7.png", "8.png", "9.png", "10.png", "11.png", "12.png", "13.png", "14.png", "15.png", "16.png", "17.png", "18.png", "19.png", "20.png", "21.png", "22.png", "23.png", "24.png", "25.png", "26.png", "27.png", "28.png", "29.png", "30.png", "31.png", "32.png", "33.png", "34.png", "35.png", "36.png", "00.png"];
            var green = ["0.png", "00.png"];
        }

        var color = args[0];
        if (color != null) color = color.toLowerCase();
        var money = args[1];
        var lowerLimit = client.config.lowerLimit;

        if ((color == null) || (money == null)) return message.reply(`\nDo '${client.config.botSettings.prefix}roulette black 500' or '${client.config.botSettings.prefix}roulette 0 500' to set your bet\n'${client.config.botSettings.prefix}help roulette' for more Info`);
        if (isNaN(money)) return message.reply(`\n${money} is not a valid number`);
        if (money < 0) return message.reply(`\nthat doesn't seem to be right`);
        if (money < lowerLimit) return message.reply(`\nYou need at least ${lowerLimit}$ to play`);
        if (balance < money) return message.reply(`\nYou don't have enough Money to play\ncurrent balance: ${number.format(balance)}$`);
        if (client.config.europeanRoulette == 1 && color == '00') return message.reply(`\n00 is not available in european mode`);

        function embedPost(color, post) {
            const embedPost = new Discord.MessageEmbed()
                .setColor(color)
                .setAuthor(`${user} played roulette`, message.author.avatarURL())
                .setDescription(post)
                .attachFiles(`./assets/roulette/${roll}`)
                .setImage(`attachment://${roll}`)
            message.channel.send(embedPost).catch(console.error);
            return;
        }
        
        //veto function
        //set name of current game
        client.db.setEco(SID, message.author, `veto_game`, client.commands.get(message.content.slice(prefix.length).trim().split(/ +/g).shift().toLowerCase()).name);
        //set win/loss
        //set +/- depending on the outcome of the game
        function veto_stats(win, status) {
            //veto
            client.db.setEco(SID, message.author, `veto_money`, win);
            client.db.setEco(SID, message.author, `veto_status`, status);
        }
        

        if ((color >= 0) && (color <= 36)) {
            var roll = symbol[Math.floor(Math.random() * symbol.length)];
            if (roll == `${color}.png`) {
                var win = money * 35;
                win = client.db.prestigeMoney(SID, message.author, win);
                client.db.addEco(SID, message.author, `money`, win);
                client.db.addEco(SID, message.author, `xp`, client.db.prestigeXP(SID, message.author, 10));
                var post = `You won ${number.format(win)}$`;
                //veto
                veto_stats(win, '+');
            } else
            if (roll != `${color}.png`) {
                var win = money;
                client.db.subEco(SID, message.author, `money`, win);
                client.db.addEco(SID, message.author, `xp`, client.db.prestigeXP(SID, message.author, 5));
                var post = `You lost ${number.format(win)}$`;
                //veto
                veto_stats(win, '-');
            }
            if ((red.includes(`${roll}`) == true) && ((color >= 0) && (color <=36))) {
                embedPost(0xa82418, post);
                return;
            } else
            if ((black.includes(`${roll}`) == true)  && ((color >= 0) && (color <=36))) {
                embedPost(0x141414, post);
                return;
            } else
            if ((green.includes(`${roll}`) == true)  && ((color >= 0) && (color <=36))) {
                embedPost(0x719b1f, post);
                return;
            }
        } else
        if ((color == "red") || (color == "black") || (color == "green")) {
            var roll = symbol[Math.floor(Math.random() * symbol.length)];
            if (red.includes(`${roll}`) == true) {
                var postColor = "red";
                if (color == postColor) {
                    var win = money;
                    win = client.db.prestigeMoney(SID, message.author, win);
                    client.db.addEco(SID, message.author, `money`, win);
                    client.db.addEco(SID, message.author, `xp`, client.db.prestigeXP(SID, message.author, 10));
                    var post = `You Won ${number.format(win)}$`;
                    //veto
                    veto_stats(win, '+');
                } else
                if (color != postColor) {
                    var win = money;
                    client.db.subEco(SID, message.author, `money`, win);
                    client.db.addEco(SID, message.author, `xp`, client.db.prestigeXP(SID, message.author, 5));
                    var post = `You lost ${number.format(win)}$`;
                    //veto
                    veto_stats(win, '-');
                }
                embedPost(0xa82418, post);
                return;
            } else
            if (black.includes(`${roll}`) == true) {
                var postColor = "black";
                if (color == postColor) {
                    var win = money;
                    win = client.db.prestigeMoney(SID, message.author, win);
                    client.db.addEco(SID, message.author, `money`, win);
                    client.db.addEco(SID, message.author, `xp`, client.db.prestigeXP(SID, message.author, 10));
                    var post = `You Won ${number.format(win)}$`;
                    //veto
                    veto_stats(win, '+');
                } else
                if (color != postColor) {
                    var win = money;
                    client.db.subEco(SID, message.author, `money`, win);
                    client.db.addEco(SID, message.author, `xp`, client.db.prestigeXP(SID, message.author, 5));
                    var post = `You lost ${number.format(win)}$`;
                    //veto
                    veto_stats(win, '-');
                }
                embedPost(0x141414, post);
                return;
            } else
            if (green.includes(`${roll}`) == true) {
                var postColor = "green";
                if (color == postColor) {
                    var win = money * 35;
                    win = client.db.prestigeMoney(SID, message.author, win);
                    client.db.addEco(SID, message.author, `money`, win);
                    client.db.addEco(SID, message.author, `xp`, client.db.prestigeXP(SID, message.author, 10));
                    var post = `You Won ${number.format(win)}$`;
                    //veto
                    veto_stats(win, '+');
                } else
                if (color != postColor) {
                    var win = money;
                    client.db.subEco(SID, message.author, `money`, win);
                    client.db.addEco(SID, message.author, `xp`, client.db.prestigeXP(SID, message.author, 5));
                    var post = `You lost ${number.format(win)}$`;
                    //veto
                    veto_stats(win, '-');
                }
                embedPost(0x719b1f, post);
                return;
            }
        } return message.reply(`\n${color} is not a valid color/number\nplease play with red, black, green or a number between 0/00 and 36`);
    }
}
