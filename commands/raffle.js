module.exports = {
    name: "raffle",
    cooldown: 3,
    run (client, message, args, cooldownTime) {
        if(cooldownTime != 0) return message.reply(`please wait ${cooldownTime} to play again`);
        const Discord = require('discord.js');
        var number = Intl.NumberFormat();
        if (message.channel.type != 'dm') SID = message.guild.id;
        if (message.channel.type == 'dm') SID = 'dm';
        let raffleCost = 500;
        let category = [1, 10, 25, 50, 100, 250, 750];        //die letzte Position ist die Anzahl der Nieten
        let categoryPrice = [1000000, 50000, 25000, 5000, 1000, 500];       
        var user = message.author.username;            
        let raffleboxTempString = client.db.getEco(SID, client.user, "raffle");
        let rafflebox = [];

        //veto function
        //set name of current game
        client.db.setEco(SID, message.author, `veto_game`, client.commands.get(message.content.slice(prefix.length).trim().split(/ +/g).shift().toLowerCase()).name);
        //set win/loss
        //set +/- depending on the outcome of the game
        function veto_stats(win, status) {
            //veto
            client.db.setEco(SID, message.author, `veto_money`, win);
            client.db.setEco(SID, message.author, `veto_status`, status);
        }
        
        if(! raffleboxTempString) {//.length) {
        for(i=0; i < categoryPrice.length; i++) {
        for(j=0; j < category[i]; j++) {
                    rafflebox.push(categoryPrice[i]);
                }
            }
            
            for(i=0; i < category[category.length-1]; i++) {
                rafflebox.push(0);
            }
            client.db.setEco(SID, client.user, "raffle", rafflebox.join("-"));
        }
        else {
            rafflebox = raffleboxTempString.split("-");
        }

        if(client.db.subEco(SID, message.author, "money", raffleCost) == false) {
            message.reply(`You don't have enough money\nYou have ${number.format(client.db.getEco(SID, message.author, "money"))}$`);
        }
        else {
            let raffleticketIndex = Math.floor(Math.random() * rafflebox.length)
            let raffleticket = rafflebox[raffleticketIndex];
            rafflebox.splice(raffleticketIndex, 1);
            client.db.setEco(SID, client.user, "raffle", rafflebox.join("-"));

            if (raffleticket == categoryPrice[0]) {
                raffleticket = client.db.prestigeMoney(SID, message.author, raffleticket);
                raffleCost = client.db.prestigeMoney(SID, message.author, raffleCost);
                client.db.addEco(SID, message.author, `xp`, client.db.prestigeXP(SID, message.author, 500));
                var embedPost = new Discord.MessageEmbed()
                    .setColor(0xc99400)
                    .setAuthor(`${user} pulled a raffle ticket`, message.author.avatarURL())
                    .addField(`Main Price!`, `You Won ${number.format(raffleticket)}$`)
                message.channel.send(embedPost).catch(console.error);
                return;
            }
            if (raffleticket != 0) {
                raffleticket = client.db.prestigeMoney(SID, message.author, raffleticket);
                raffleCost = client.db.prestigeMoney(SID, message.author, raffleCost);
                client.db.addEco(SID, message.author, "money", raffleticket);
                client.db.addEco(SID, message.author, "money", raffleCost);
                client.db.addEco(SID, message.author, `xp`, client.db.prestigeXP(SID, message.author, 10));
                var embedPost = new Discord.MessageEmbed()
                    .setColor(0x719b1f)
                    .setAuthor(`${user} pulled a raffle ticket`, message.author.avatarURL())
                    .setDescription(`You Won ${number.format(raffleticket)}$`)
                message.channel.send(embedPost).catch(console.error);
                return;
            }
            else {;
                client.db.addEco(SID, message.author, `xp`, client.db.prestigeXP(SID, message.author, 5));
                var embedPost = new Discord.MessageEmbed()
                    .setColor(0xa82418)
                    .setAuthor(`${user} pulled a raffle ticket`, message.author.avatarURL())
                    .addField(`-${number.format(raffleCost)}$`, `You pulled a blank`)
                message.channel.send(embedPost).catch(console.error);
                return;
            }
        }
    }
}