module.exports = {
    name: "lotto",
    cooldown: 21600,
    run (client, message, args, cooldownTime) {
        if (cooldownTime <= 60 && cooldownTime != 0) {
            message.reply(`\nPlease wait ${cooldownTime} seconds to tip again`);
            return;
        }
        if (cooldownTime <= 3600 && cooldownTime != 0) {
            cooldownTime = cooldownTime /60;
            message.reply(`\nPlease wait ${cooldownTime.toFixed(0)} minutes to tip again`);
            return;
        }
        if (cooldownTime > 3600 && cooldownTime != 0) {
            cooldownTime = cooldownTime / 60 / 60;
            message.reply(`\nPlease wait ${cooldownTime.toFixed(1)} hours to tip again`);
            return;
        }

        
        const Discord = require('discord.js');
        var number = Intl.NumberFormat();
        if (message.channel.type != 'dm') SID = message.guild.id;
        if (message.channel.type == 'dm') SID = 'dm';
        var tip1 = args[0];
        var tip2 = args[1];
        var tip3 = args[2];
        var tip4 = args[3];
        var tip5 = args[4];
        var tip6 = args[5];
        var lotto = 1000;
        var multi = client.config.lottoMultiplicator;
        var money = client.db.getEco(SID, message.author, `money`);


        //veto function
        //set name of current game
        client.db.setEco(SID, message.author, `veto_game`, client.commands.get(message.content.slice(prefix.length).trim().split(/ +/g).shift().toLowerCase()).name);
        //set win/loss
        //set +/- depending on the outcome of the game
        function veto_stats(win, status) {
            //veto
            client.db.setEco(SID, message.author, `veto_money`, win);
            client.db.setEco(SID, message.author, `veto_status`, status);
        }
        

        if (money < lotto) {
            message.reply(`\nYou need at least ${lotto}$ to tip`);
            client.db.setEco(SID, message.author, `cooldown_lotto`, ``);
            return;
        }
        if (args[0] == null || args[1] == null || args[2] == null || args[3] == null || args[4] == null) {
            message.reply(`\nYou must tip at least 5 Numbers`);
            client.db.setEco(SID, message.author, `cooldown_lotto`, ``);
            return;
        }
        if (tip1 < 1 || tip1 > 50 || tip2 < 1 || tip2 > 50 || tip3 < 1 || tip3 > 50 || tip4 < 1 || tip4 > 50 || tip5 < 1 || tip5 > 50) {
            message.reply(`\nTips must be bewteen 1 and 50`);
            client.db.setEco(SID, message.author, `cooldown_lotto`, ``);
            return;
        }
        if (isNaN(tip1) || isNaN(tip2) || isNaN(tip3) || isNaN(tip4) || isNaN(tip5) || isNaN(tip6)) {
            message.reply(`\nTips must be bewteen 1 and 50`);
            client.db.setEco(SID, message.author, `cooldown_lotto`, ``);
            return;
        }
        if (tip1 == tip2 || tip1 == tip3 || tip1 == tip4 || tip1 == tip5 || tip2 == tip3 || tip2 == tip4 || tip2 == tip5 || tip3 == tip4 || tip3 == tip5 || tip4 == tip5) {
            message.reply(`\nYou can't tip a number twice`);
            client.db.setEco(SID, message.author, `cooldown_lotto`, ``);
            return;
        }
        if (tip6 < 1 || tip6 > 10) {
            message.reply(`\nThe Supernumber must be between 1 and 10`);
            client.db.setEco(SID, message.author, `cooldown_lotto`, ``);
            return;
        }
        message.delete();


        //generate Numbers
        var getDate = client.db.getEco(SID, client.user, `lotto_date`);
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0');
        var yyyy = today.getFullYear();
        today = dd + mm + yyyy;

        if (today != getDate) {
            min = Math.ceil(1);
            max = Math.floor(50);
            superMin = Math.ceil(1);
            superMax = Math.floor(10);
            var num1 = client.db.getEco(SID, client.user, `lotto_num1`);
            var num2 = client.db.getEco(SID, client.user, `lotto_num2`);
            var num3 = client.db.getEco(SID, client.user, `lotto_num3`);
            var num4 = client.db.getEco(SID, client.user, `lotto_num4`);
            var num5 = client.db.getEco(SID, client.user, `lotto_num5`);
            var num6 = client.db.getEco(SID, client.user, `lotto_num6`);

            client.db.setEco(SID, client.user, `lotto_num1`, Math.floor(Math.random() * (max - min) + min));
            client.db.setEco(SID, client.user, `lotto_num2`, Math.floor(Math.random() * (max - min) + min));
            client.db.setEco(SID, client.user, `lotto_num3`, Math.floor(Math.random() * (max - min) + min));
            client.db.setEco(SID, client.user, `lotto_num4`, Math.floor(Math.random() * (max - min) + min));
            client.db.setEco(SID, client.user, `lotto_num5`, Math.floor(Math.random() * (max - min) + min));
            client.db.setEco(SID, client.user, `lotto_num6`, Math.floor(Math.random() * (superMax - superMin) + superMin));
            client.db.setEco(SID, client.user, `lotto_date`, today);
        }

        //get generated Numbers
        var num1 = client.db.getEco(SID, client.user, `lotto_num1`);
        var num2 = client.db.getEco(SID, client.user, `lotto_num2`);
        var num3 = client.db.getEco(SID, client.user, `lotto_num3`);
        var num4 = client.db.getEco(SID, client.user, `lotto_num4`);
        var num5 = client.db.getEco(SID, client.user, `lotto_num5`);
        var num6 = client.db.getEco(SID, client.user, `lotto_num6`);

        //check Numbers and Tips
        var correctTips = 0;
        var superTip = 0;
        if (tip1 == num1 || tip1 == num2 || tip1 == num3 || tip1 == num4 || tip1 == num5) correctTips++;
        if (tip2 == num1 || tip2 == num2 || tip2 == num3 || tip2 == num4 || tip2 == num5) correctTips++;
        if (tip3 == num1 || tip3 == num2 || tip3 == num3 || tip3 == num4 || tip3 == num5) correctTips++;
        if (tip4 == num1 || tip4 == num2 || tip4 == num3 || tip4 == num4 || tip4 == num5) correctTips++;
        if (tip5 == num1 || tip5 == num2 || tip5 == num3 || tip5 == num4 || tip5 == num5) correctTips++;
        if (tip6 == num6) superTip++;
        

        //Add XP
        var win = 0;
        if (correctTips == 0) {
            if (superTip == 1) client.db.addEco(SID, message.author, `xp, 5`);
            client.db.addEco(SID, message.author, `xp`, client.db.prestigeXP(SID, message.author, 5));
        }

        if (correctTips == 1) {
            var win = 1000;
            if (superTip == 1) {
                win = win * multi;
                client.db.addEco(SID, message.author, `xp`, client.db.prestigeXP(SID, message.author, 5));
            }
            client.db.addEco(SID, message.author, `xp`, client.db.prestigeXP(SID, message.author, 10));
        }

        if (correctTips == 2) {
            var win = 10000;
            if (superTip == 1) {
                win = win * mutli;
                client.db.addEco(SID, message.author, `xp`, client.db.prestigeXP(SID, message.author, 5));
            }
            client.db.addEco(SID, message.author, `xp`, 20);
        }
        
        if (correctTips == 3) {
            var win = 100000;
            if (superTip == 1) {
                win = win * multi;
                client.db.addEco(SID, message.author, `xp`, client.db.prestigeXP(SID, message.author, 5));
            }
            client.db.addEco(SID, message.author, `xp`, 30);
        }
        
        if (correctTips == 4) {
            var win = 1000000;
            if (superTip == 1) {
                win = win * multi;
                client.db.addEco(SID, message.author, `xp`, client.db.prestigeXP(SID, message.author, 5));
            }
            client.db.addEco(SID, message.author, `xp`, 40);
        }
        
        if (correctTips == 5) {
            var win = 10000000;
            if (superTip == 1) {
                win = win * multi;
                client.db.addEco(SID, message.author, `xp`, client.db.prestigeXP(SID, message.author, 5));
            }
            client.db.addEco(SID, message.author, `xp`, 50);
        }


        //Add win and send embed
        if (win > 0 && superTip == 1) {
            win = client.db.prestigeMoney(SID, message.author, win);
            client.db.addEco(SID, message.author, `money`, win);
            if (correctTips == 1) var num = 'number';
            if (correctTips > 1) var num = 'numbers';
            var embedPost = new Discord.MessageEmbed()
                .setColor(0x718f32)
                .setAuthor(`${message.author.username} played lotto`, message.author.avatarURL())
                .addField(`You won ${number.format(win)}$`, `${correctTips} correct ${num} + x${multi} multiplicator`)
            message.channel.send(embedPost);
            return;
        }

        if (win > 0) {
            win = client.db.prestigeMoney(SID, message.author, win);
            client.db.addEco(SID, message.author, `money`, win);
            if (correctTips == 1) var num = 'number';
            if (correctTips > 1) var num = 'numbers';
            var embedPost = new Discord.MessageEmbed()
                .setColor(0x718f32)
                .setAuthor(`${message.author.username} played lotto`, message.author.avatarURL())
                .addField(`You won ${number.format(win)}$`, `${correctTips} correct ${num}`)
            message.channel.send(embedPost);
            return;

        }

        if (win == 0) {
            client.db.subEco(SID, message.author, `money`, lotto);
            var embedPost = new Discord.MessageEmbed()
                .setColor(0xa82418)
                .setAuthor(`${message.author.username} played lotto`, message.author.avatarURL())
                .addField(`-${number.format(lotto)}$`, `You can tip again in 6h`)
            message.channel.send(embedPost);
            return;
        }

    }
}
