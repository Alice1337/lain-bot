module.exports = {
    name: "insurance",
    cooldown: 5,
    run (client, message, args, cooldownTime) {
        if(cooldownTime != 0) return message.reply(`please wait ${cooldownTime} to play again`);

        var number = Intl.NumberFormat();
        if (message.channel.type != 'dm') SID = message.guild.id;
        if (message.channel.type == 'dm') SID = 'dm';
        var dateNow = new Date();
        var buy = args[0];
        if (buy != null) buy = buy.toLowerCase()
        var insuranceConfig = client.config.insurance;
        var maxExchangeRate = insuranceConfig.exchangeRate;
        var penaltyInterval = insuranceConfig.penaltyInterval;  //Intervallzeit bis Strafe verringert wird (in Stunden)
        var penaltyValue = insuranceConfig.penaltyValue;        // = -5%
        var penaltyDecrease = insuranceConfig.penaltyDecrease;  //mit jedem Intervall wird die Strafe um diesen Wert reduziert
        var userBalance = client.db.getEco(SID, message.author, "money");
        var userInsurance = client.db.getEco(SID, message.author, "insurance");

        if (args[0] == 'buy') {
            let userInsurance = client.db.getEco(SID, message.author, "insurance");
            let insuranceConfig = client.config.insurance;
            let maxExchangeRate = insuranceConfig.exchangeRate;
            let minAmount = insuranceConfig.minAmount;

            if(userInsurance == null || userInsurance == 0) {
                let localUserInsurance = {
                    insurance: 0,
                    exchangeRate: maxExchangeRate,
                    date: 0,
                }
                userInsurance = localUserInsurance;
            }
            //#region Wechselkurs und Fehlerabfrage
            if(args[1] == null) {
                message.reply(`\nDo '${client.config.botSettings.prefix}buy insurance 5000' to buy an insurance\nTo check your conditions do '${client.config.botSettings.prefix}insurance'\n'${client.config.botSettings.prefix}help insurance' for more Info`);
                return;
            }
            if(args[1] == isNaN) {
                message.reply(`${args[1]} is not a valid number`);
                return;
            }
            if(args[1] < minAmount) {
                message.reply(`The minimum amount for insurance is ${number.format(minAmount)}$`);
                return;
            }
            //#endregion

            //#region Auswertung und übergabe
            if(userInsurance.insurance == 0) {
                userInsurance.insurance = Math.floor(args[1] * userInsurance.exchangeRate);
                userInsurance.date = new Date();

                if(client.db.subEco(SID, message.author, "money", args[1]) == false) {
                    message.reply(`\nYou don't have enough money\nYour current balance is ${number.format(client.db.getEco(SID, message.author, "money"))}$`);
                }
                else {
                    client.db.setEco(SID, message.author, "insurance", userInsurance);
                    message.reply(`You've bought an insurance over ${number.format(userInsurance.insurance)}$`);
                }
            }
            else {
                if(client.db.subEco(SID, message.author, "money", args[1]) == false) {
                    message.reply(`\nYou don't have enough money\nYour current balance is ${number.format(client.db.getEco(SID, message.author, "money"))}$`);
                }
                else {
                    let tempInsuranceAdd = Math.floor(args[1] * userInsurance.exchangeRate);
                    userInsurance.insurance = userInsurance.insurance + tempInsuranceAdd;
                    client.db.setEco(SID, message.author, "insurance", userInsurance);
                    
                    message.reply(`You've added ${number.format(tempInsuranceAdd)}$ to your insurance\nYou now have an insurance over ${number.format(userInsurance.insurance)}$`);
                }
            }return;
        }

        if(userBalance > 0) {
            if(userInsurance == null) {
                return message.reply(`\nYou don't have an insurance yet\nGet one with '${client.config.botSettings.prefix}insurance buy money'\nYour current exchange rate is: 1:${maxExchangeRate}`);
            }
            else if (userInsurance.insurance == 0){
                return message.reply(`\nYou don't have an insurance yet\nGet one with '${client.config.botSettings.prefix}insurance buy money'\nYour current exchange rate is: 1:${maxExchangeRate}`);
            }
            else {
                return message.reply(`\nYou've got a ${number.format(userInsurance.insurance)}$ insurance\nYour current exchange rate is: 1:${userInsurance.exchangeRate}`);
            }
            
        }

        if(userInsurance == null || userInsurance.insurance == 0) {
            return message.reply(`You don't have an insurance`);
        }
        //#region Strafwerte
        var buyDate = new Date(userInsurance.date);
        var elapsedTime = Math.abs(dateNow - buyDate) / (60*60*1000);
        var penaltyCounter = Math.floor(elapsedTime / penaltyInterval);
        penaltyValue = penaltyValue + penaltyDecrease * penaltyCounter;

        if(userInsurance.exchangeRate + penaltyValue > maxExchangeRate) {
            userInsurance.exchangeRate = maxExchangeRate;
        }
        else {
            userInsurance.exchangeRate = parseFloat((userInsurance.exchangeRate + penaltyValue).toFixed(2));
        }
        //#endregion

        client.db.addEco(SID, message.author, "money", userInsurance.insurance);
        message.reply(`You got ${number.format(userInsurance.insurance)}$ back from insurance`);
        userInsurance.insurance = 0;
        client.db.setEco(SID, message.author, "insurance", userInsurance);
        
    }
}