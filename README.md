<h1>How to Configure</h1>
Install nodejs and npm on your OS<br>
Install the required node modules with
<div class="highlight highlight-source-shell"><pre>npm run deps</pre></div>
Create a token.json file in the ./configs folder
<div class="highlight highlight-source-shell"><pre>{<br>	"token": "botTokenGoesHere"<br>}</pre><br></div>
Create a new role named 'Casino' on your server with default permissions<br>
<br>
In ./configs/config.json you need to:<br>
 • Change the botID to the ID of your bot<br>
 • Change the adminID to your ID to get access to the admin commands<br>
<br>
You can also easily:<br>
 • Disable requiring a Casino-role on your Server<br>
 • Change the Main and .dev prefixes<br>
 • Disable User DM's to the bot<br>
 • Change the lowerLimit for playing<br>
 • Change the Roulette mode between american and european<br>
 • Change the lotto multiplicator<br>
 • Configure insurance.js<br>
 And some other things<br>
<br>
If you append .dev to your bots name the bot will listen to the .dev prefixes<br>
useful for testing and bugfixing<br>
<br>
To start the bot navigate into the bot folder and run
<div class="highlight highlight-source-shell"><pre>npm run bot</pre></div>
You might need to create a database folder in case it's not created automatically
<br>
<br>
![alt text](assets/readme/help.png "help")<br>
![alt text](assets/readme/roulette.png "roulette")<br>
![alt text](assets/readme/spin.png "spin")<br>
![alt text](assets/readme/blackjack.png "redblack")<br>
![alt text](assets/readme/stats.png "stats")<br>