const Discord = require("discord.js");
const fs = require("fs");
const client = new Discord.Client();

const message = require("./events/message");
const count = require("console");
const token = require("./configs/token.json");
client.config = require("./configs/config.json");
client.package = require("./package.json");
client.db = require("./ecoSystem.js");


client.on("ready", () => {
    console.log(`Logged in as ${client.user.tag}!`);

    client.user.setPresence({
        status: `online`, game: {
            name: `${client.config.botSettings.prefix}help`, 
            type: `PLAYING`
        }
    })
    if (client.user.username.endsWith('.dev')) {
        client.user.setPresence({
            status: `online`, game: {
                name: `${client.config.botSettings.prefixDev}help`, 
                type: `PLAYING`
            }
        })
    }
});


fs.readdir("./events/", (err, files) => {
    if (err) return console.error(err);
    files.forEach(file => {
        if (!file.endsWith(".js")) return;
        const event = require(`./events/${file}`);
        let eventName = file.split(".")[0];
        client.on(eventName, event.bind(null, client));
        delete require.cache[require.resolve(`./events/${file}`)];
    })
});


client.commands = new Discord.Collection();

fs.readdir("./commands/", (err,files) => {
    if (err) return console.error(err);
    files.forEach(file => {
        if (!file.endsWith(".js")) return;
        let props = require(`./commands/${file}`);
        let commandName = file.split(".")[0];
        console.log(`Attempting to load command ${commandName}`);
        client.commands.set(commandName, props);
    });
});


client.login(token.token);