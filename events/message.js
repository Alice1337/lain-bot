module.exports = (client, message) => {
    if (message.channel.type != 'dm') SID = message.guild.id;
    if (message.channel.type == 'dm') SID = 'dm';
    prefix = client.config.botSettings.prefix;
    if (client.user.username.endsWith('.dev')) {
        prefix = client.config.botSettings.prefixDev;
    }
    if (message.author.bot) return;
    if (message.channel.type == "dm" && message.author.id !== client.config.privileges.adminID && client.config.botSettings.DM_allow == 0) return message.reply(`DM's are disabled`);
    
    
    //Check for casino-role
    if (message.channel.type != "dm" && message.content.startsWith(prefix) && !message.member.roles.cache.some(r=>["Casino"].includes(r.name))) {
        const args = message.content.slice(prefix.length).trim().split(/ +/g);
        const command = args.shift().toLowerCase();
        const cmd = client.commands.get(command);

        if ((command == 'casino' && client.db.getEco(SID, message.author, `casino_ban`) != 1) || command == 'help' || command == 'rip' || command == 'ez' || command == 'gg') return cmd.run(client, message, args);
        if (client.db.getEco(SID, message.author, `casino_ban`) == 1) return message.reply(`You are not permitted to enter the casino`);
        if (command != 'casino' && client.config.botSettings.requireCasinoRole == 1) return message.reply("\nYou need to be in the Casino to start playing\nType `casino` to get started");
    }


    //run command
    if (message.content.startsWith(prefix)) {
        const args = message.content.slice(prefix.length).trim().split(/ +/g);
        const command = args.shift().toLowerCase();
        const cmd = client.commands.get(command);
        client.db.setEco(SID, message.author, `last_command`, command);
        if (!message.channel.name.includes(client.config.botSettings.channelName) && client.config.botSettings.requireCasinoChannel == 1 && !client.user.username.endsWith('.dev')) {
            if (command != 'help' && command != 'casino') {
                return;
            }
        }

        if (!cmd) return;
        const now = Date.now();
        const cooldownTime = 0;
        const cooldownAmount = cmd.cooldown * 1000;
        const lastExecution = client.db.getEco(SID, message.author, `cooldown_${cmd.name}`);

        //Anticheat
        var maxRepeats = client.config.LainAntiCheat.maxRepeats;
        var cooldownAccuracy = client.config.LainAntiCheat.cooldownAccuracy;
        var LAC_command = client.db.getEco(SID, message.author, `LAC_command`);
        var LAC_cooldown = client.db.getEco(SID, message.author, `LAC_cooldown`);
        var LAC_cooldownSecondsLast = client.db.getEco(SID, message.author, `LAC_cooldownSeconds`);

        client.db.setEco(SID, message.author, `LAC_command`, command);
        client.db.setEco(SID, message.author, `LAC_cooldown`, now);

        //Detect repeating commands
        if (LAC_command == command) {
            //Calculate time gone by between two repeating commands
            LAC_cooldownSeconds = (now-LAC_cooldown)/1000;
            LAC_cooldownSeconds = LAC_cooldownSeconds.toFixed(cooldownAccuracy);
            client.db.setEco(SID, message.author, `LAC_cooldownSeconds`, LAC_cooldownSeconds);

            if (LAC_command != command || LAC_cooldownSecondsLast != LAC_cooldownSeconds) client.db.setEco(SID, message.author, `LAC_commandRepeat`, 1)

            //Detect repeating cooldowns
            if (LAC_cooldownSeconds == LAC_cooldownSecondsLast) {
                client.db.addEco(SID, message.author, `LAC_commandRepeat`, 1);

                if (client.db.getEco(SID, message.author, `LAC_commandRepeat`) == maxRepeats) {
                    message.channel.send(`${message.author} has been kicked by Anticheat`);
                    message.author.send(`You have been kicked by Anticheat`);
                    return message.member.roles.remove(message.guild.roles.cache.find(role => role.name === "Casino")).catch(console.error);
                }
            }
        }

        if(lastExecution !== null) {
            const expirationTime = lastExecution + cooldownAmount;
            if(now < expirationTime) {
                const seconds = (expirationTime - now) / 1000;
                const cooldownTime = `${seconds.toFixed(1)}`;
                return cmd.run(client, message, args, cooldownTime);
            }
        }
        client.db.setEco(SID, message.author, `cooldown_${cmd.name}`, now);
        cmd.run(client, message, args, cooldownTime);
    }
}